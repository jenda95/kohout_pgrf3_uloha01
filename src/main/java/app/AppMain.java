package app;

import app.audio.Audio;
import app.gui.GUIaudio;
import app.gui.GUIaudioThread;
import app.gui.GuiSwitchThread;
import app.gui.GuiThread;
import app.window.WindowMain;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class AppMain {

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
        // Start pgrf
//        WindowMain windowMain = new WindowMain();

        GuiSwitchThread guiSwitchThread = new GuiSwitchThread("switch");
        guiSwitchThread.start();

//        GuiThread guiThread = new GuiThread("guiWindow", windowMain);
//        guiThread.start();
//        GUIaudioThread guIaudioThread = new GUIaudioThread("guiAudioWindow", windowMain);
//        guIaudioThread.start();
//        windowMain.run();
        // Start audio
//        Audio audio = new Audio();
//        System.out.println(audio.getAudioFile().exists());
//        audio.pumpIt();
    }
}
