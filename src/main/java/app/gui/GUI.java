/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.util.UtilFunctions;
import app.window.ExternalListenerUtils;
import app.window.WindowMain;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.text.NumberFormat;
import java.util.Hashtable;

public class GUI extends JFrame {

    private JTextField pointSize;
    private JTextField speed;
    private JTextField gridWidth;
    private JTextField gridHeight;
    private JComboBox<String> perspModeBox;
    private JComboBox<String> mainModeBox;
    private JComboBox<String> gridRenderModeBox;
    private JComboBox<String> functionModeBox;
    private JComboBox<String> textureBox;
    private JComboBox<String> surfaceBox;
    private JComboBox<String> modelBox;

    private JSlider ambient;
    private JSlider diffuse;
    private JSlider specular;
    private static int FPS_MIN=0, FPS_MAX=100;
    private JCheckBox spotlight;

    private JSlider blur;
    private JCheckBox ao;

    private JPanel infoToolbar1;
    private JPanel infoToolbar2;
    private JPanel perspectiveModeToolbar;
    private JPanel mainModeToolbar;
    private JPanel gridToolbar;
    private JPanel aoBlurToolbar;
    private JPanel lightToolbar;
    private JPanel functionToolbar;
    private JPanel textureToolbar;
    private JPanel surfaceToolbar;
    private JPanel colorToolbar;
    private JPanel modelToolbar;

    private JTextField red;
    private JTextField green;
    private JTextField blue;

    private WindowMain windowMain;
    private ExternalListenerUtils externalListenerUtils;

    private int initWidth = 800;
    private int initHeight = 420;

    public GUI(WindowMain windowMain) throws HeadlessException {

        setLocationRelativeTo(null);
        setSize(initWidth, initHeight);
        this.windowMain = windowMain;
        this.externalListenerUtils = new ExternalListenerUtils(this.windowMain);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }

        initInputs();
        initListeners();

    }

    private void initInputs() {
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        NumberFormat format = NumberFormat.getInstance();

        // Setting up new integer formater for integer required inputs
        NumberFormatter integerFormatter = new NumberFormatter(format);
        integerFormatter.setValueClass(Integer.class);
        integerFormatter.setMinimum(0);
        integerFormatter.setMaximum(Integer.MAX_VALUE);
        integerFormatter.setAllowsInvalid(false);
        // If you want the value to be committed on each keystroke instead of focus lost
        integerFormatter.setCommitsOnValidEdit(true);

        //Info toolbar
        this.infoToolbar1 = new JPanel();
        this.getContentPane().add(infoToolbar1);
        JLabel infoLabel1 = new JLabel("CAPSLOCK = switches between camera and light control");
        infoToolbar1.add(infoLabel1);

        this.infoToolbar2 = new JPanel();
        this.getContentPane().add(infoToolbar2);
        JLabel infoLabel2 = new JLabel("TAB = reset camera, WASD = walk, L_SHIFT = up, L_CTRL = down");
        infoToolbar2.add(infoLabel2);

        // Initializing inputs
        this.perspectiveModeToolbar = new JPanel();
        this.getContentPane().add(perspectiveModeToolbar);

        JLabel perspModeLabel = new JLabel("Select perspective mode: ");
        String[] perspModes = new String[]{ "Perspective", "Orthogonal"};
        this.perspModeBox = new JComboBox<String>(perspModes);

        perspectiveModeToolbar.add(perspModeLabel);
        perspectiveModeToolbar.add(perspModeBox);


        this.mainModeToolbar = new JPanel();
        this.getContentPane().add(mainModeToolbar);

        JLabel mainModeLabel = new JLabel("Select main mode: ");
        String[] mainModes = new String[]{ "Grid functions", "Objects.obj"};
        this.mainModeBox = new JComboBox<String>(mainModes);

        mainModeToolbar.add(mainModeLabel);
        mainModeToolbar.add(mainModeBox);

        // Grid inputs
        this.gridToolbar = new JPanel();
        this.getContentPane().add(gridToolbar);

        JLabel width = new JLabel("Width:");
        this.gridWidth = new JTextField("100",5);

        JLabel height = new JLabel("Height:");
        this.gridHeight = new JTextField("100", 5);

        JLabel pointSizeLabel = new JLabel("Point size: ");
        this.pointSize = new JTextField("1", 3);

        String[] gridRenderModes = new String[]{ "Triangles", "Lines", "Points"};
        this.gridRenderModeBox = new JComboBox<String>(gridRenderModes);

        JLabel speedLabel = new JLabel("Speed: ");
        this.speed = new JTextField("1", 2);

        gridToolbar.add(width);
        gridToolbar.add(gridWidth);
        gridToolbar.add(height);
        gridToolbar.add(gridHeight);
        gridToolbar.add(pointSizeLabel);
        gridToolbar.add(pointSize);
        gridToolbar.add(gridRenderModeBox);
        gridToolbar.add(speedLabel);
        gridToolbar.add(speed);

        // Light toolbar
        this.lightToolbar = new JPanel();
        this.getContentPane().add(lightToolbar);

        int ambientInit = 30;
        int diffuseInit = 50;
        int specularInit = 80;

        ambient = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, ambientInit);
        Hashtable<Integer, JLabel> labelAmbient =
                new Hashtable<Integer, JLabel>();
        labelAmbient.put(new Integer( 50 ),
                new JLabel("Ambient") );
        ambient.setLabelTable(labelAmbient);
        ambient.setPaintLabels(true);

        diffuse = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, diffuseInit);
        Hashtable<Integer, JLabel> labelDiffuse =
                new Hashtable<Integer, JLabel>();
        labelDiffuse.put(new Integer( 50 ),
                new JLabel("Diffuse") );
        diffuse.setLabelTable(labelDiffuse);
        diffuse.setPaintLabels(true);

        specular = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, specularInit);
        Hashtable<Integer, JLabel> labelSpecular =
                new Hashtable<Integer, JLabel>();
        labelSpecular.put(new Integer( 50 ),
                new JLabel("Specular") );
        specular.setLabelTable(labelSpecular);
        specular.setPaintLabels(true);

        spotlight = new JCheckBox("Spotlight");

        lightToolbar.add(ambient);
        lightToolbar.add(diffuse);
        lightToolbar.add(specular);
        lightToolbar.add(spotlight);

        // AO blur
        this.aoBlurToolbar = new JPanel();
        this.getContentPane().add(aoBlurToolbar);

        int blurInit = 16;
        int blurMin = 0;
        int blurMax = 30;

        blur = new JSlider(JSlider.HORIZONTAL, blurMin, blurMax, blurInit);
        Hashtable<Integer, JLabel> labelBlur =
                new Hashtable<Integer, JLabel>();
        labelBlur.put(new Integer( 50 ),
                new JLabel("Blur") );
        blur.setLabelTable(labelBlur);
        blur.setPaintLabels(true);

        ao = new JCheckBox("Ambient oclussion");
        ao.setSelected(true);

        aoBlurToolbar.add(ao);
        aoBlurToolbar.add(blur);

        // Function control
        this.functionToolbar = new JPanel();
        this.getContentPane().add(functionToolbar);

        JLabel functionModeLabel = new JLabel("Select function: ");
        String[] functionModes = new String[]{ "Koule", "Vejir", "Vaza", "Valec", "Plocha", "Plocha pohyb"};
        this.functionModeBox = new JComboBox<String>(functionModes);

        functionToolbar.add(functionModeLabel);
        functionToolbar.add(functionModeBox);

        this.surfaceToolbar = new JPanel();
        this.getContentPane().add(surfaceToolbar);

        JLabel surfaceBoxLabel = new JLabel("Select surface mode: ");
        String[] surfaceModes = new String[]{ "Texture", "XYZ Color", "Color", "Normal vertex", "Texture + Normal"};
        this.surfaceBox = new JComboBox<String>(surfaceModes);

        surfaceToolbar.add(surfaceBoxLabel);
        surfaceToolbar.add(surfaceBox);

        this.textureToolbar = new JPanel();
        this.getContentPane().add(textureToolbar);

        JLabel textureBoxLabel = new JLabel("Select texture: ");
        String[] textureModes = UtilFunctions.loadTextures();
        this.textureBox = new JComboBox<String>(textureModes);

        textureToolbar.add(textureBoxLabel);
        textureToolbar.add(textureBox);

        this.colorToolbar = new JPanel();
        this.getContentPane().add(colorToolbar);

        JLabel redLabel = new JLabel("Red: ");
        this.red = new JTextField("255",5);

        JLabel greenLabel = new JLabel("Green: ");
        this.green = new JTextField("255",5);

        JLabel blueLabel = new JLabel("Blue: ");
        this.blue = new JTextField("255",5);

        this.colorToolbar.add(redLabel);
        this.colorToolbar.add(red);
        this.colorToolbar.add(greenLabel);
        this.colorToolbar.add(green);
        this.colorToolbar.add(blueLabel);
        this.colorToolbar.add(blue);


        this.modelToolbar = new JPanel();
        this.getContentPane().add(modelToolbar);
        modelToolbar.setVisible(false);

        JLabel modeLabel = new JLabel("Select object: ");
        String[] modelModes = new String[]{"Objekt", "DoubleScene"};
        this.modelBox = new JComboBox<String>(modelModes);

        modelToolbar.add(modeLabel);
        modelToolbar.add(modelBox);


        colorToolbar.setVisible(false);
        setVisible(true);
    }
    private void initListeners() {
        System.out.println("Initializing listeners....");
        this.gridWidth.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeWidth((Integer.parseInt(gridWidth.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeWidth((Integer.parseInt(gridWidth.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeWidth((Integer.parseInt(gridWidth.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });
        this.gridHeight.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeHeight((Integer.parseInt(gridHeight.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeHeight((Integer.parseInt(gridHeight.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeHeight((Integer.parseInt(gridHeight.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });

        this.pointSize.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changePointSize((Integer.parseInt(pointSize.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changePointSize((Integer.parseInt(pointSize.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changePointSize((Integer.parseInt(pointSize.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });

        this.speed.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeSpeed((Integer.parseInt(speed.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeSpeed((Integer.parseInt(speed.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeSpeed((Integer.parseInt(speed.getText())));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });
        this.ao.addChangeListener(e -> externalListenerUtils.changeAO(ao.isSelected()));
        this.blur.addChangeListener(e -> externalListenerUtils.changeBlur(blur.getValue()));
        this.spotlight.addChangeListener(e -> externalListenerUtils.changeSpotlight(spotlight.isSelected()));
        this.ambient.addChangeListener(e -> externalListenerUtils.changeAmbient(ambient.getValue()));
        this.diffuse.addChangeListener(e -> externalListenerUtils.changeDiffuse(diffuse.getValue()));
        this.specular.addChangeListener(e -> externalListenerUtils.changeSpecular(specular.getValue()));
        this.perspModeBox.addActionListener(e -> externalListenerUtils.changePerspMode(perspModeBox.getSelectedItem().toString()));
        this.mainModeBox.addActionListener(e -> this.setupUItems(mainModeBox.getSelectedIndex()));
        this.gridRenderModeBox.addActionListener(e -> externalListenerUtils.changeRenderMode(gridRenderModeBox.getSelectedItem().toString()));
        this.functionModeBox.addActionListener(e -> externalListenerUtils.changeFunctionMode(functionModeBox.getSelectedItem().toString()));
        this.textureBox.addActionListener(e -> externalListenerUtils.changeTexture(textureBox.getSelectedItem().toString()));
        this.surfaceBox.addActionListener(e -> this.setUpSurfaceGUItems(this.surfaceBox.getSelectedIndex()));
        this.modelBox.addActionListener(e -> externalListenerUtils.changeFunctionMode(modelBox.getSelectedItem().toString()));

        this.red.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeRed(Integer.parseInt(red.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeRed(Integer.parseInt(red.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeRed(Integer.parseInt(red.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });

        this.green.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeGreen(Integer.parseInt(green.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeGreen(Integer.parseInt(green.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeGreen(Integer.parseInt(green.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });

        this.blue.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeBlue(Integer.parseInt(blue.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void removeUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeBlue(Integer.parseInt(blue.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
            public void insertUpdate(DocumentEvent e) {
                try {
                    externalListenerUtils.changeBlue(Integer.parseInt(blue.getText()));
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }
        });

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                    System.exit(0);
            }
        });


    }

    private void setupUItems(int mode) {
//        grid, function, surface, texture
        switch(mode) {
            case 0:
                gridToolbar.setVisible(true);
                functionToolbar.setVisible(true);
                surfaceToolbar.setVisible(true);
                textureToolbar.setVisible(true);
                lightToolbar.setVisible(true);
                modelToolbar.setVisible(false);
                colorToolbar.setVisible(false);
                aoBlurToolbar.setVisible(true);
                setSize(initWidth, initHeight);
                externalListenerUtils.changeFunctionMode(functionModeBox.getSelectedItem().toString());
                break;
            case 1:
                gridToolbar.setVisible(false);
                functionToolbar.setVisible(false);
                surfaceToolbar.setVisible(false);
                textureToolbar.setVisible(false);
                modelToolbar.setVisible(true);
                colorToolbar.setVisible(false);
                lightToolbar.setVisible(false);
                aoBlurToolbar.setVisible(false);
                setSize(600, initHeight-220);
                externalListenerUtils.changeFunctionMode(modelBox.getSelectedItem().toString());
                break;
        }
    }

    private void setUpSurfaceGUItems(int surfaceMode) {
        switch(surfaceMode) {
//            "Texture", "XYZ Color", "Color", "Normal vertex", "Coordinates", "All"
            case 0:
                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
                textureToolbar.setVisible(true);
                colorToolbar.setVisible(false);
                setSize(initWidth, initHeight);
                break;
            case 1:
            case 3:
                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
                textureToolbar.setVisible(false);
                colorToolbar.setVisible(false);
                setSize(initWidth, initHeight-40);
                break;
            case 2:
                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
                textureToolbar.setVisible(false);
                colorToolbar.setVisible(true);
                setSize(initWidth, initHeight);
                break;
            case 4:
                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
                break;
        }
    }
}
