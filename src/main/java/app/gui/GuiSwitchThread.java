/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.AppMain;
import app.window.WindowMain;
import app.window.WindowMainAudio;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class GuiSwitchThread extends Thread{

    public static final int FIRST_TASK = 1;
    public static final int SECOND_TASK = 2;

    public int choosenTask = 0;

    private Thread t;
    private String threadName;

    public GuiSwitchThread(String name) {
        threadName = name;
        System.out.println("Creating " +  threadName );
    }

    public void run() {
        GuiSwitch guiSwitch = new GuiSwitch(this);
        while(choosenTask == 0){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        guiSwitch.setVisible(false);
        try {
            runTask();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }

    private void runTask() throws IOException, UnsupportedAudioFileException {
        switch (choosenTask) {
            case FIRST_TASK:
                System.out.println("Starting task 1 ....");
                WindowMain windowMain = new WindowMain();
                GuiThread guiThread = new GuiThread("guiWindow", windowMain);
                guiThread.start();
                windowMain.run();
                break;
            case SECOND_TASK:
                System.out.println("Starting task 2 ....");
                WindowMainAudio windowMainAudio = new WindowMainAudio();
                GUIaudioThread guIaudioThread = new GUIaudioThread("guiAudioWindow", windowMainAudio);
                guIaudioThread.start();
//                windowMainAudio.run();
                break;
        }
    }


}