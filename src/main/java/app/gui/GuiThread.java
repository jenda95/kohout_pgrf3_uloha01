/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.window.WindowMain;

public class GuiThread extends Thread{
    private Thread t;
    private String threadName;
    private WindowMain windowMain;

    public GuiThread(String name, WindowMain windowMain) {
        threadName = name;
        System.out.println("Creating " +  threadName );
        this.windowMain = windowMain;
    }

    public void run() {
        GUI gui = new GUI(this.windowMain);
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
