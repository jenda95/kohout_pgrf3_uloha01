/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.AppMain;
import app.audio.AudioManager;
import app.window.ExternalListenerUtils;
import app.window.WindowMain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GuiSwitch extends JFrame{

    private GuiSwitchThread listener;
    private ExternalListenerUtils externalListenerUtils;

    private int initWidth = 200;
    private int initHeight = 90;

    private JPanel switchToolbar;
    private JButton firstTask;
    private JButton secondTask;

    public GuiSwitch(GuiSwitchThread listener) throws HeadlessException {

        setLocationRelativeTo(null);
        setSize(initWidth, initHeight);
        this.listener = listener;
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }

        initInputs();
        initListeners();

    }

    private void initInputs() {
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        // switcher toolbar
        switchToolbar = new JPanel();
        this.getContentPane().add(switchToolbar);
        firstTask = new JButton("Uloha 1");
        secondTask = new JButton("Uloha 2");
        switchToolbar.add(firstTask);
        switchToolbar.add(secondTask);

        setVisible(true);
    }
    private void initListeners() {
        System.out.println("Initializing listeners....");
        this.firstTask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("uloha 1");
                listener.choosenTask = GuiSwitchThread.FIRST_TASK;
            }
        });
        this.secondTask.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("uloha 2");
                listener.choosenTask = GuiSwitchThread.SECOND_TASK;
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(0);
            }
        });

    }
}


