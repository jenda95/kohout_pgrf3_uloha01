/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.util.UtilFunctions;
import app.window.ExternalListenerUtils;
import app.window.WindowMain;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import app.audio.AudioManager;
import app.window.WindowMainAudio;
import com.github.psambit9791.wavfile.WavFileException;
import com.sun.tools.javac.Main;

public class GUIaudio extends JFrame {

    private AudioManager audioManager = new AudioManager(this);

//    private String recordAbsoluteFilepath = "/home/jk/audio/record.wav";
//    private String kickAbsoluteFilepath = "/home/jk/audio/kick.wav";
//    private String snareAbsoluteFilepath = "/home/jk/audio/snare.wav";
//    private String hihatAbsoluteFilepath = "/home/jk/audio/hihat.wav";
//
//    private String outputAbsolutePath = "/home/jk/audio-output5/";

    private String recordAbsoluteFilepath = "";
    private String kickAbsoluteFilepath = "";
    private String snareAbsoluteFilepath = "";
    private String hihatAbsoluteFilepath = "";

    private String outputAbsolutePath = "";


    private JPanel recordInputToolbar;
    private JButton record;
    private JLabel recordLabel;

    private JPanel kickInputToolbar;
    private JButton kick;
    private JLabel kickLabel;

    private JPanel snareInputToolbar;
    private JButton snare;
    private JLabel snareLabel;

    private JPanel hihatInputToolbar;
    private JButton hihat;
    private JLabel hihatLabel;

    private JPanel outputDirToolbar;
    private JButton outputDir;
    private JLabel outputDirLabel;

    private JPanel filterToolbar;
    private JButton filter;
    public JLabel filterLabel;

    private JPanel checkBoxesToolbar;
    private JCheckBox kickCheck;
    private JCheckBox snareCheck;
    private JCheckBox hihatCheck;

    private JPanel playerToolbar;
    private JButton playThaScheisse;

    private FileChooser fileChooser;

    private WindowMainAudio windowMainAudio;
    private ExternalListenerUtils externalListenerUtils;

    private int initWidth = 800;
    private int initHeight = 420;

    public boolean corelationDone = false;
    private boolean isPlaying = false;

    Clip clip;
    AudioInputStream inputStream;


    public GUIaudio(WindowMainAudio windowMainAudio) throws HeadlessException, LineUnavailableException, IOException, UnsupportedAudioFileException {
        clip = AudioSystem.getClip();

        System.out.println(outputAbsolutePath + "signal.wav");
        setLocationRelativeTo(null);
        setSize(initWidth, initHeight);
        this.windowMainAudio = windowMainAudio;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }

        initInputs();
        initListeners();
    }

    private void initInputs() {
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        //audio input toolbars
        recordInputToolbar = new JPanel();
        this.getContentPane().add(recordInputToolbar);
        record = new JButton("Choose record");
        recordLabel = new JLabel("No file chosen", JLabel.RIGHT);
        recordLabel.setPreferredSize(new Dimension(600, 15));
        recordInputToolbar.add(recordLabel);
        recordInputToolbar.add(record);

        kickInputToolbar = new JPanel();
        this.getContentPane().add(kickInputToolbar);
        kick = new JButton("Choose kick");
        kickLabel = new JLabel("No file chosen", JLabel.RIGHT);
        kickLabel.setPreferredSize(new Dimension(600, 15));
        kickInputToolbar.add(kickLabel);
        kickInputToolbar.add(kick);

//        snareInputToolbar = new JPanel();
//        this.getContentPane().add(snareInputToolbar);
//        snare = new JButton("Choose snare");
//        snareLabel = new JLabel("No file chosen", JLabel.RIGHT);
//        snareLabel.setPreferredSize(new Dimension(600, 15));
//        snareInputToolbar.add(snareLabel);
//        snareInputToolbar.add(snare);
//
//        hihatInputToolbar = new JPanel();
//        this.getContentPane().add(hihatInputToolbar);
//        hihat = new JButton("Choose hihat");
//        hihatLabel = new JLabel("No file chosen", JLabel.RIGHT);
//        hihatLabel.setPreferredSize(new Dimension(600, 15));
//        hihatInputToolbar.add(hihatLabel);
//        hihatInputToolbar.add(hihat);

        outputDirToolbar = new JPanel();
        this.getContentPane().add(outputDirToolbar);
        outputDir = new JButton("Choose output dir");
        outputDirLabel = new JLabel("No dir choosen", JLabel.RIGHT);
        outputDirLabel.setPreferredSize(new Dimension(600, 15));
        outputDirToolbar.add(outputDirLabel);
        outputDirToolbar.add(outputDir);

        filterToolbar = new JPanel();
        this.getContentPane().add(filterToolbar);
        filter = new JButton("Filter patterns!");
        filterLabel = new JLabel(".....");
        filterLabel.setPreferredSize(new Dimension(600, 15));
        filterToolbar.add(filterLabel);
        filterToolbar.add(filter);

        // player toolbar
//        Dimension layout = new Dimension(100, 50);
//        checkBoxesToolbar = new JPanel();
//        this.getContentPane().add(checkBoxesToolbar);
//        kickCheck = new JCheckBox("kick");
//        kickCheck.setPreferredSize(layout);
//        kickCheck.setSelected(true);
//        snareCheck = new JCheckBox("snare");
//        snareCheck.setPreferredSize(layout);
//        snareCheck.setSelected(true);
//        hihatCheck = new JCheckBox("hihat");
//        hihatCheck.setPreferredSize(layout);
//        hihatCheck.setSelected(true);
//        checkBoxesToolbar.add(kickCheck);
//        checkBoxesToolbar.add(snareCheck);
//        checkBoxesToolbar.add(hihatCheck);

        playerToolbar = new JPanel();
        this.getContentPane().add(playerToolbar);
        playThaScheisse = new JButton("►");
        playerToolbar.add(playThaScheisse);

        setVisible(true);
    }

    private void initListeners() {
        System.out.println("Initializing listeners....");
        this.record.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooser = new FileChooser();
                recordAbsoluteFilepath = fileChooser.getAbsoluteFilePath();
                System.out.println("Record absolute file path: " + recordAbsoluteFilepath);
                recordLabel.setText(recordAbsoluteFilepath);
            }
        });
        this.kick.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooser = new FileChooser();
                kickAbsoluteFilepath = fileChooser.getAbsoluteFilePath();
                System.out.println("Kick absolute file path: " + kickAbsoluteFilepath);
                kickLabel.setText(kickAbsoluteFilepath);
            }
        });
//        this.snare.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                fileChooser = new FileChooser();
//                snareAbsoluteFilepath = fileChooser.getAbsoluteFilePath();
//                System.out.println("Snare absolute file path: " + snareAbsoluteFilepath);
//                snareLabel.setText(snareAbsoluteFilepath);
//            }
//        });
//        this.hihat.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                fileChooser = new FileChooser();
//                hihatAbsoluteFilepath = fileChooser.getAbsoluteFilePath();
//                System.out.println("Hihat absolute file path: " + hihatAbsoluteFilepath);
//                hihatLabel.setText(hihatAbsoluteFilepath);
//            }
//        });
        this.outputDir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooser = new FileChooser(1);
                outputAbsolutePath = fileChooser.getAbsoluteFilePath();
                System.out.println("Output absolute file path: " + outputAbsolutePath);
                outputDirLabel.setText(outputAbsolutePath);
            }
        });
        this.filter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!recordAbsoluteFilepath.equals("") &&
                        !kickAbsoluteFilepath.equals("")
//                        !snareAbsoluteFilepath.equals("") &&
//                        !hihatAbsoluteFilepath.equals("") &&
//                        !outputAbsolutePath.equals("")
                ) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                    audioManager.crossCorelation(recordAbsoluteFilepath, kickAbsoluteFilepath, "kick", outputAbsolutePath);
//                    audioManager.crossCorelation(recordAbsoluteFilepath, snareAbsoluteFilepath, "snare", outputAbsolutePath);
//                    audioManager.crossCorelation(recordAbsoluteFilepath, hihatAbsoluteFilepath, "hihat", outputAbsolutePath);
                    audioManager.startCheckingProgress();
                } else {
                    filterLabel.setText("Choose files!");
                }
            }
        });
        this.playThaScheisse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!isPlaying) {
                    playThaScheisse.setText("■");
                    isPlaying = true;
                    try {
                        playMusic();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    } catch (LineUnavailableException lineUnavailableException) {
                        lineUnavailableException.printStackTrace();
                    } catch (UnsupportedAudioFileException unsupportedAudioFileException) {
                        unsupportedAudioFileException.printStackTrace();
                    }

                } else {
                    playThaScheisse.setText("►");
                    isPlaying = false;
                    stopMusic();
                }
            }
        });

        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                System.exit(0);
            }
        });


    }

    public void showPlayerAndVisualisation() throws IOException, WavFileException {
        windowMainAudio.setPathToRecord(outputAbsolutePath+"signal.wav");
        windowMainAudio.setPathToKick(outputAbsolutePath+"kick-full.wav");
        windowMainAudio.initKickSample();
//        windowMainAudio.setPathToSnare(outputAbsolutePath+"snare-full.wav");
//        windowMainAudio.setPathToHihat(outputAbsolutePath+"hihat-full.wav");
        windowMainAudio.run();
    }

    private void playMusic() throws IOException, LineUnavailableException, UnsupportedAudioFileException {
        inputStream = AudioSystem.getAudioInputStream(new File(outputAbsolutePath + "signal.wav"));
        clip.open(inputStream);
        windowMainAudio.initCounters();
        clip.start();
        windowMainAudio.setFunctionMode(1);
        this.isPlaying = true;
    }

    private void stopMusic()
    {
        clip.close();
        windowMainAudio.setFunctionMode(0);
        this.isPlaying = false;
    }

    private void setupUItems(int mode) {
////        grid, function, surface, texture
//        switch(mode) {
//            case 0:
//                gridToolbar.setVisible(true);
//                functionToolbar.setVisible(true);
//                surfaceToolbar.setVisible(true);
//                textureToolbar.setVisible(true);
//                lightToolbar.setVisible(true);
//                modelToolbar.setVisible(false);
//                colorToolbar.setVisible(false);
//                aoBlurToolbar.setVisible(true);
//                setSize(initWidth, initHeight);
//                externalListenerUtils.changeFunctionMode(functionModeBox.getSelectedItem().toString());
//                break;
//            case 1:
//                gridToolbar.setVisible(false);
//                functionToolbar.setVisible(false);
//                surfaceToolbar.setVisible(false);
//                textureToolbar.setVisible(false);
//                modelToolbar.setVisible(true);
//                colorToolbar.setVisible(false);
//                lightToolbar.setVisible(false);
//                aoBlurToolbar.setVisible(false);
//                setSize(600, initHeight-220);
//                externalListenerUtils.changeFunctionMode(modelBox.getSelectedItem().toString());
//                break;
//        }
    }

//    private void setUpSurfaceGUItems(int surfaceMode) {
//        switch(surfaceMode) {
////            "Texture", "XYZ Color", "Color", "Normal vertex", "Coordinates", "All"
//            case 0:
//                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
//                textureToolbar.setVisible(true);
//                colorToolbar.setVisible(false);
//                setSize(initWidth, initHeight);
//                break;
//            case 1:
//            case 3:
//                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
//                textureToolbar.setVisible(false);
//                colorToolbar.setVisible(false);
//                setSize(initWidth, initHeight-40);
//                break;
//            case 2:
//                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
//                textureToolbar.setVisible(false);
//                colorToolbar.setVisible(true);
//                setSize(initWidth, initHeight);
//                break;
//            case 4:
//                externalListenerUtils.changeSurfaceMode(surfaceBox.getSelectedItem().toString());
//                break;
//        }
//    }
}

