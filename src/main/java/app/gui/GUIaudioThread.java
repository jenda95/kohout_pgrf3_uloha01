/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.gui;

import app.window.WindowMain;
import app.window.WindowMainAudio;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class GUIaudioThread extends Thread{
    private Thread t;
    private String threadName;
    private WindowMainAudio windowMainAudio;

    public GUIaudioThread(String name, WindowMainAudio windowMainAudio) {
        threadName = name;
        System.out.println("Creating " +  threadName );
        this.windowMainAudio = windowMainAudio;
    }

    public void run() {
        try {
            GUIaudio guIaudio = new GUIaudio(this.windowMainAudio);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}