package app.gui;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

public class FileChooser {

    private JFileChooser jfc;
    private String absoluteFilePath;


    // int returnValue = jfc.showSaveDialog(null);

    public FileChooser() {
        jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            System.out.println(selectedFile.getAbsolutePath());
            this.absoluteFilePath = selectedFile.getAbsolutePath();
        }
    }

    public FileChooser(int mode) {
        jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            if (jfc.getSelectedFile().isDirectory()) {
                File selectedDir = jfc.getSelectedFile();
                System.out.println("You selected the directory: " + selectedDir);
                this.absoluteFilePath = selectedDir.getAbsolutePath()+"/";
            }
        }
    }

    public String getAbsoluteFilePath() {
        return absoluteFilePath;
    }
}
