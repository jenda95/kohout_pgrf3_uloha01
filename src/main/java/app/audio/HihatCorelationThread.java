package app.audio;

import com.github.psambit9791.wavfile.WavFileException;

import java.io.IOException;

public class HihatCorelationThread extends Thread{
    private AudioManager audioManager;
    private Thread t;
    private String threadName;
    private String name;
    private String outputAbsolutePath;

    private String signal;
    private String kernel;

    public HihatCorelationThread(AudioManager audioManager, String name, String signal, String kernel, String outputAbsolutePath) {
        this.audioManager = audioManager;
        this.signal = signal;
        this.kernel = kernel;
        this.name = name;
        this.outputAbsolutePath = outputAbsolutePath;
        threadName = name + " thread";
        System.out.println("Creating " +  threadName + "...." );
    }

    public void run() {
        audioManager.hihatCorelation = false;
        System.out.println("Startting corelation: " + threadName);
        try {
            Audio audio = new Audio(name, signal, kernel, outputAbsolutePath);
            audio.corelate("full", 0.01);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WavFileException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Done corelation: " + threadName);
        audioManager.hihatCorelation = true;
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
