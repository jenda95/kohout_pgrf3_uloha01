package app.audio;

import app.gui.GUIaudio;
import com.github.psambit9791.wavfile.WavFileException;

import java.io.IOException;

public class AudioManager {

    GUIaudio listener;

    public boolean kickCorelation = true;
    public boolean snareCorelation = true;
    public boolean hihatCorelation = true;

    public AudioManager(GUIaudio guIaudio)
    {
        this.listener = guIaudio;
    }

    public void crossCorelation(String signal, String kernel, String name, String outputAbsolutePath) {
        switch (name) {
            case "kick":
                KickCorelationThread kickCorelationThread = new KickCorelationThread(this, name, signal, kernel, outputAbsolutePath);
                kickCorelationThread.start();
                break;
            case "snare":
                SnareCorelationThread snareCorelationThread = new SnareCorelationThread(this, name, signal, kernel, outputAbsolutePath);
                snareCorelationThread.start();
                break;
            case "hihat":
                HihatCorelationThread hihatCorelationThread = new HihatCorelationThread(this, name, signal, kernel, outputAbsolutePath);
                hihatCorelationThread.start();
                break;
        }
    }

    public void startCheckingProgress() {
        CheckingProgressThread checkingProgressThread = new CheckingProgressThread(this, "checking");
        checkingProgressThread.start();
    }

    public boolean corelationDone()
    {
        boolean corelationDone = false;
//        if(kickCorelation && snareCorelation && hihatCorelation) {
//            corelationDone = true;
//        } else {
//            listener.filterLabel.setText("Filtering in progress!");
//        }
        if(kickCorelation) {
            corelationDone = true;
        } else {
            listener.filterLabel.setText("Filtering in progress!");
        }
        return corelationDone;
    }

    public void updateStatus(String status) {
        listener.filterLabel.setText(status);
    }

    public void setCorelationDone (boolean corelationDone) throws IOException, WavFileException {
        listener.corelationDone = corelationDone;
        listener.showPlayerAndVisualisation();
    }

}
