package app.audio;

import com.github.psambit9791.jdsp.io.Wav;
import com.github.psambit9791.jdsp.misc.Plotting;
import com.github.psambit9791.jdsp.signal.CrossCorrelation;
import com.github.psambit9791.wavfile.WavFileException;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;



public class Audio {

//    int channels = 2;
//    int samplingRate = 44100;
    private static final int CHANNELS = 2;
    private static final int SAMPLING_RATE = 44100;
    private static final int SAMPLING_RATE2 = 88200;

    private Wav kernel = new Wav();
    private Wav signal = new Wav();

    private String name;
    private String  outputAbsolutePath;

    private static byte[] toByteArray(double value) {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    private static double toDouble(byte[] bytes) {
        return ByteBuffer.wrap(bytes).getDouble();
    }

    private static double[] toDoubleArray(byte[] byteArray) {
        double result[] = new double[byteArray.length];
        for(int i = 0; i < byteArray.length; i++) {
            result[i] = byteArray[i];
        }
        return result;
    }

    private static void printDoubles(double[] array){
        for (double item : array) {
            System.out.print(item);
            System.out.print("|");
        }
    }

    private static void printDoublesLines(double[] array){
        for (double item : array) {
            System.out.println(item);
        }
    }

    private static void printBytes(byte[] array){
        for (byte item : array) {
            System.out.print(item);
            System.out.print("|");
        }
    }

    private void saveArrayToCSV(double[] array, String name) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter(name+".csv"));
        StringBuilder sb = new StringBuilder();
        // Append strings from array
        for (Double element : array) {
            sb.append(element);
            sb.append("\n");
        }
        br.write(sb.toString());
        br.close();
    }

    private double[] multiplySignalArray(double[] array, double multConst) {
        double[] result = new double[array.length];
        for(int i = 0; i < array.length; i++) {
            result[i] = array[i]*multConst;
        }
        return result;
    }

    private double[] reduceOutFromCC(double[] array) {
        double[] result = new double[array.length];
        for(int i = 0; i < array.length; i++) {
            result[i] = array[i]/100000;
        }
        return result;
    }

    private double[] powerTheSignal(double[] array) {
        double[] result = new double[array.length];
        for(int i = 0; i < array.length; i++) {
            result[i] = array[i]*array[i]*array[i];
        }
        return result;
    }

    private double[] array2DTo1DArray(double[][] inarray) {
        ArrayList list = new ArrayList();
        double[] result = new double[inarray.length*inarray[0].length];
        for(int i = 0; i < inarray.length; i++) {
            for(int j = 0; j < inarray[0].length; j++) {
                list.add(inarray[i][j]);
            }
        }
        for(int i = 0; i < list.size(); i++) {
            result[i] = (double)list.get(i);
        }
        return result;
    }

    private double[][] array1DTo2DArray(double[] inarray) {
        double[][] result = new double[inarray.length/2][2];
        for(int i = 0; i < inarray.length/2; i=i+2) {
            result[i][0] = inarray[i];
            result[i][1] = inarray[i+1];
        }
        return result;
    }

    private double[] createTimeArray(int arrayLenght) {
        double [] result = new double[arrayLenght];
        for(int i = 0; i < arrayLenght; i++) {
            result[i] = i;
        }
        return result;
    }

    public Audio(String name, String signal, String kernel, String outputAbsolutePath) throws IOException, WavFileException {
        this.name = name;
        this.outputAbsolutePath = outputAbsolutePath;
        System.out.println("******"+this.name+"******");
        System.out.println("Init function ...");
        System.out.println("Reading audiofiles ...");
        this.kernel.readWav(kernel);
        this.signal.readWav(signal);
        System.out.println("Files read.");

    }

    public void corelate(String mode, double amplitudeMultiplication) throws IOException, WavFileException {
        // mode -> full, same, valid
        String filename = outputAbsolutePath+name+"-"+mode;

        System.out.println("******");
        System.out.println("Getting audio data ...");
        double[][] kernelData2D = kernel.getData("double");
        double[][] signalData2D = signal.getData("double");


        System.out.println("******");
        System.out.println("Converting data to one dimensional array ...");
        double[] kernelData = array2DTo1DArray(kernelData2D);
        double[] signalData = array2DTo1DArray(signalData2D);

        Wav signalOutWav = new Wav();
//        audioWrite.putData(signalOut, samplingRate, "double", filename + ".wav");
        signalOutWav.putData(array1DTo2DArray(signalData), SAMPLING_RATE2, "double", outputAbsolutePath+"signal.wav");

        System.out.println("******");
        System.out.println("Saving decoded signal and kernel to csv ...");
        saveArrayToCSV(signalData, outputAbsolutePath+"signal");
//        saveArrayToCSV(kernelData, filename+"kernel");


        System.out.println("*******************************************");
        System.out.println("Cross corelation processing ...");
        // full, same, valid
        CrossCorrelation cc = new CrossCorrelation(signalData, kernelData);
        double[] out = cc.crossCorrelate(mode);
        out = multiplySignalArray(out, amplitudeMultiplication);
        saveArrayToCSV(out, filename);
        System.out.println("Cross corelation finished: " + filename + ".csv");

        System.out.println("Saving cross-corelated audio file .... " + filename + ".wav");
        try{
            double[][] signalOut = array1DTo2DArray(out);
            Wav audioWrite = new Wav();
            audioWrite.putData(signalOut, SAMPLING_RATE2, "double", filename + ".wav");
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("*********************** SUMARY **********************");
        System.out.println("Signal lenght: "+ signalData.length);
        System.out.println("CC result lenght: "+ out.length);

//        System.out.println("Plotting: "+ filename);
//        Plotting fig = new Plotting(600, 500, name, "Samples", "Signal");
//        fig.initialisePlot();
//
//        double[] timeArray = createTimeArray(out.length);
//        fig.addPoints(name, timeArray, out, 'x');
//
//        String outputPlot = outputAbsolutePath+name+".png";
//        fig.plot();
//        fig.saveAsPNG(outputPlot);
    }

    public Audio() throws IOException, UnsupportedAudioFileException {

//        try {
//
//            System.out.println("******");
//            System.out.println("Init function ...");
//            System.out.println("Reading audiofiles ...");
//            kernel.readWav("/home/jk/skola/kohout_pgrf3_uloha01/src/main/resources/audio/kick.wav");
//            signal.readWav("/home/jk/skola/kohout_pgrf3_uloha01/src/main/resources/audio/record.wav");
//            System.out.println("Files read.");
//
//            System.out.println("******");
//            System.out.println("Getting audio data ...");
//            double[][] kernelData2D = kernel.getData("double");
//            double[][] signalData2D = signal.getData("double");
//
//            System.out.println("******");
//            System.out.println("Converting data to one dimensional array ...");
//            double[] kernelData = array2DTo1DArray(kernelData2D);
//            double[] signalData = array2DTo1DArray(signalData2D);
//
//            System.out.println("******");
//            System.out.println("Saving decoded signal and kernel to csv ...");
//            saveArrayToCSV(signalData, "signal");
//            saveArrayToCSV(kernelData, "kernel");
//
//
//            System.out.println("*******************************************");
//            System.out.println("Cross corelation processing ...");
//            // full, same, valid
//            String mode = "full";
//            CrossCorrelation cc = new CrossCorrelation(signalData, kernelData);
//            double[] out = cc.crossCorrelate(mode);
//            out = multiplySignalArray(out, 0.01);
//            saveArrayToCSV(out, mode);
//            System.out.println("Cross corelation finished: " + mode+".csv");
//
//            System.out.println("Saving cross-corelated audio file .... " +mode+".wav");
//            try{
//                double[][] signalOut = array1DTo2DArray(out);
//                Wav audioWrite = new Wav();
//                int channels = 2;
//                audioWrite.putData(signalOut, channels, "double", mode+".wav");
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//
//            System.out.println("*********************** SUMARY **********************");
//            System.out.println("Signal lenght: "+ signalData.length);
//            System.out.println("CC result lenght: "+ out.length);
//
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
    }

    public double[][] readWav(String pathToAudioFile) throws IOException, WavFileException
    {
        Wav audioFile = new Wav();
        audioFile.readWav(pathToAudioFile);
        System.out.println(audioFile.getProperties());
        return audioFile.getData("double");
    }

    public double[] downsampleAudio(double[][] audio)
    {
        double[] downsampled = new double[audio.length/2+2];
        for(int i = 0; i < audio.length; i=i+2) {
            downsampled[i/2] = audio[i][0];
        }
        return downsampled;
    }

    public double[] downsampleAudio(double[] audio)
    {
        double[] downsampled = new double[audio.length/2+2];
        for(int i = 0; i < audio.length; i=i+2) {
            downsampled[i/2] = audio[i];
        }
        return downsampled;
    }

    public double[] downsampleAudio(double[] audio, int howManyTimes, int sampleRate)
    {
        double[] downsampled = new double[audio.length/2+2];
        int newSampleRate = sampleRate;
        for(int j = 0; j < howManyTimes; j++) {
            newSampleRate = newSampleRate/2;
            if(j == 0) {
                for(int i = 0; i < audio.length; i=i+2) {
                    downsampled[i/2] = audio[i];
                }
            } else {
                double[] downsampledNew = new double[downsampled.length / 2 + 2];
                for (int i = 0; i < downsampled.length; i = i + 2) {
                    downsampledNew[i / 2] = downsampled[i];
                }
                downsampled = null;
                downsampled = downsampledNew;
            }
        }
        System.out.println("New sample rate: "+ newSampleRate);
        return downsampled;
    }




}
