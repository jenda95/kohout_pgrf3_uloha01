package app.audio;

import com.github.psambit9791.wavfile.WavFileException;

import java.io.IOException;

public class CheckingProgressThread extends Thread{
    private AudioManager audioManager;
    private Thread t;
    private String threadName;



    public CheckingProgressThread(AudioManager audioManager, String name) {
        this.audioManager = audioManager;
        threadName = name + " thread";
        System.out.println("Creating " +  threadName + "...." );
    }

    public void run() {
        //here to start corelation
        audioManager.updateStatus("Filtering in progress!");
        while(!audioManager.corelationDone()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        audioManager.updateStatus("....");
        try {
            audioManager.setCorelationDone(true);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (WavFileException e) {
            e.printStackTrace();
        }
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}

