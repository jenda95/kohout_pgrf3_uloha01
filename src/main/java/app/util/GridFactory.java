/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.util;

import lwjglutils.OGLBuffers;

public class GridFactory {

    public static OGLBuffers makeGrid(int width, int height) {

        /**
         * creates vertices
         */
        float[] vertexBufferData = new float[width*height*2];
        for(int x=0; x<width; x++) {
            for (int y = 0; y < height; y++) {
                vertexBufferData[(y * width + x) * 2] = x * 1f / (width - 1);
                vertexBufferData[(y * width + x) * 2 + 1] = y * 1f / (height - 1);
            }
        }

        /**
         * prints vertices
         */
//        for (float e:vertexBufferData) {
//            System.out.print(e + " ");
//        }
//        System.out.println();
//        for (int i = 0; i < vertexBufferData.length; i+=2) {
//            System.out.println(i+": ["+ vertexBufferData[i]+","+vertexBufferData[i+1]+"]");
//        }

        /**
         * creates indices
         */
        int[] indexBufferData = new int[(width-1)*(height-1)*3*2*2];
        int indx = 0;
        for (int j=0; j<=(height-2); j++) {
            for (int i=0; i<=(width-2); i++) {
                indexBufferData[indx++] = (j*height)+width+i;
                indexBufferData[indx++] = (j*height)+i;
                indexBufferData[indx++] = (j*height)+i+1;

                indexBufferData[indx++] = (j*height)+i;
                indexBufferData[indx++] = (j*height)+i+1;
                indexBufferData[indx++] = (j*height)+width+i;

                indexBufferData[indx++] = (j*height)+width+i+1;
                indexBufferData[indx++] = (j*height)+i+1;
                indexBufferData[indx++] = (j*height)+width+i;

                indexBufferData[indx++] = (j*height)+i+1;
                indexBufferData[indx++] = (j*height)+width+i;
                indexBufferData[indx++] = (j*height)+width+i+1;
            }
        }

        /**
         * prints indices
         */
//        for(int index:indexBufferData) {
//            System.out.print(index+" ");
//        }
//        System.out.println();
//        System.out.println(indexBufferData.length/3);

        // vertex binding description, concise version
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2),
//                new OGLBuffers.Attrib("inNormal", 3),
//                new OGLBuffers.Attrib("inTextureCoordinates", 2)
        };

        return new OGLBuffers(vertexBufferData, attributes,
                indexBufferData);

    }

    public static OGLBuffers gridGenerate(int rows, int columns) {

        int vertices = rows * columns;
        int triangles = (rows - 1) * (columns - 1) * 2;


        float[] vb = new float[vertices * 2];
        int[] ib = new int[triangles * 3];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int vbIndex1 = (i * rows + j) * 2;
                int vbIndex2 = (i * columns + j) * 2 + 1;

                vb[vbIndex1] = i / (float) (columns - 1);
                vb[vbIndex2] = j / (float) (rows - 1);
            }
        }
        int index = 0;
        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < columns - 1; j++) {
                ib[index] = j + i * columns;
                ib[index + 1] = j + 1 + (i + 1) * columns;
                ib[index + 2] = j + (i + 1) * columns;
                ib[index + 3] = j + i * columns;
                ib[index + 4] = j + 1 + i * columns;
                ib[index + 5] = j + 1 + (i + 1) * columns;
                index += 6;
            }
        }

        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2),
//                new OGLBuffers.Attrib("inNormal", 3),
                new OGLBuffers.Attrib("inTextureCoordinates", 2)
        };

        return new OGLBuffers(vb, attributes, ib);
    }

    public static OGLBuffers getQuad() {
        float[] vbData = {-1, -1, 1, -1, 1, 1, -1, 1};
        int[] ibData = {0, 1, 2, 0, 2, 3};

        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2)
        };
        return new OGLBuffers(vbData, attributes, ibData);
    }
}
