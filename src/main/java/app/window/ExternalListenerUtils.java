/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.window;

import static org.lwjgl.opengl.GL11.*;

public class ExternalListenerUtils {
    private WindowMain windowMain;

    public ExternalListenerUtils(WindowMain windowMain)
    {
        this.windowMain = windowMain;
    }
    /**
     * Answers for GUI listeners
     */
    public void changePerspMode(String perspMode) {
//        "Perspective", "Orthogonal"
        switch(perspMode){
            case "Perspective":
                windowMain.perspectiveMode = 1;
                break;
            case "Orthogonal":
                windowMain.perspectiveMode = 2;
                break;
        }
        windowMain.perspectiveModeChanged = true;
    }

    public void changeWidth(int width) {
        if(width > 2) {
            windowMain.gridWidth = width;
            windowMain.gridChanged = true;
        }
    }

    public void changeHeight(int height) {
        if(windowMain.width > 2) {
            windowMain.gridHeight = height;
            windowMain.gridChanged = true;
        }
    }

    public void changePointSize(int pointSize) {
        windowMain.pointSize = pointSize;
    }

    public void changeSpeed(double speed) {
        windowMain.timeflow = (float)speed;
    }

    public void changeAmbient(int ambientIntensity) {
        windowMain.ambientIntensity = (float)ambientIntensity/100;
    }

    public void changeDiffuse(int diffusetIntensity) {
        windowMain.diffuseIntensity = (float)diffusetIntensity/100;
    }

    public void changeSpecular(int specularIntensity) {
        windowMain.specularIntensity = (float)specularIntensity/100;

    }
    public void changeSpotlight(boolean checked) {
        windowMain.spotlight = checked;
    }

    public void changeAO(boolean checked) {
        windowMain.ao = checked;
    }

    public void changeBlur(int blurIntensity) {
        windowMain.blur = blurIntensity;
    }


    public void changeRenderMode(String renderMode) {
        System.out.println(renderMode);
        switch(renderMode) {
            case "Points":
                windowMain.renderMode = GL_POINTS;
                break;
            case "Lines":
                windowMain.renderMode = GL_LINES;
                break;
            case "Triangles":
                windowMain.renderMode = GL_TRIANGLES;
                break;
        }
        windowMain.renderModeChanged = true;
    }

    public void changeFunctionMode(String functionMode) {
        System.out.println(functionMode);
        if(windowMain.mainMode == 0 && (functionMode == "Objekt" || functionMode == "DoubleScene")) {
            windowMain.mainModeChanged = true;
        } else if(windowMain.mainMode == 1 && (functionMode == "Koule" || functionMode == "Vejir"
                || functionMode == "Vaza" || functionMode == "Valec"
                || functionMode == "Plocha" || functionMode == "Plocha pohyb")) {
            windowMain.mainModeChanged = true;
        }
        switch(functionMode) {
            case "Koule":
                windowMain.mainMode = 0;
                windowMain.functionMode = 2;
                break;
            case "Vejir":
                windowMain.mainMode = 0;
                windowMain.functionMode = 3;
                break;
            case "Vaza":
                windowMain.mainMode = 0;
                windowMain.functionMode = 4;
                break;
            case "Valec":
                windowMain.mainMode = 0;
                windowMain.functionMode = 5;
                break;
            case "Plocha":
                windowMain.mainMode = 0;
                windowMain.functionMode = 0;
                break;
            case "Plocha pohyb":
                windowMain.mainMode = 0;
                windowMain.functionMode = 1;
                break;
            case "Objekt":
                windowMain.mainMode = 1;
                windowMain.functionMode = 6;
                break;
            case "DoubleScene":
                windowMain.mainMode = 1;
                windowMain.functionMode = 7;
                break;
        }
        System.out.println(windowMain.functionMode);
        windowMain.functionModeChanged = true;
    }

    public void changeTexture(String texture) {
        System.out.println(texture);
        windowMain.textureName = texture;
        windowMain.textureChanged = true;
    }

    public void changeSurfaceMode(String surfaceMode){
        System.out.println(surfaceMode);
//        XYZ Color
        switch(surfaceMode) {
            case "Texture":
                windowMain.surfaceMode = 0;
                break;
            case "Color":
                windowMain.surfaceMode = 1;
                break;
            case "Normal vertex":
                windowMain.surfaceMode = 2;
                break;
            case "Texture + Normal":
                windowMain.surfaceMode = 3;
                break;
            case "All":
                windowMain.surfaceMode = 4;
                break;
            case "XYZ Color":
                windowMain.surfaceMode = 5;
                break;

        }
        System.out.println(windowMain.surfaceMode);
        windowMain.surfaceModeChanged = true;
    }

    public void changeRed(int red){
        if(red >= 0 && red <= 255) {
            windowMain.colors[0] = (float) 1 / 255 * red;
        }
    }

    public void changeGreen(int green){
        if(green >= 0 && green <= 255) {
            windowMain.colors[1] = (float) 1/255*green;
        }
    }

    public void changeBlue(int blue){
        if(blue >= 0 && blue <= 255) {
            windowMain.colors[2] = (float) 1/255*blue;
        }
    }
}
