//package app.window;
//
//import app.util.GridFactory;
//import app.util.RandomTextureGenerator;
//import lwjglutils.*;
//import org.lwjgl.BufferUtils;
//import org.lwjgl.glfw.*;
//import org.lwjgl.opengl.GL;
//import org.lwjgl.system.MemoryStack;
//import transforms.Mat3RotZ;
//import transforms.Mat4PerspRH;
//import transforms.Vec3D;
//
//import java.io.IOException;
//import java.nio.DoubleBuffer;
//import java.nio.IntBuffer;
//
//import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
//import static org.lwjgl.glfw.GLFW.*;
//import static org.lwjgl.glfw.GLFW.glfwShowWindow;
//import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL20.*;
//import static org.lwjgl.opengl.GL20.glUniform1i;
//import static org.lwjgl.system.MemoryStack.stackPush;
//import static org.lwjgl.system.MemoryUtil.NULL;
//
//public class InitWindowUtilsAudio {
//    private WindowMainAudio windowMainAudio;
//
//    public InitWindowUtilsAudio(WindowMainAudio windowMainAudio)
//    {
//        this.windowMainAudio = windowMainAudio;
//    }
//
//    public void completeInit() throws IOException
//    {
//        basicInit();
//        initShaders();
//        initLocationVars();
//        initSceneVars();
//        initScene();
//        initShaderVars();
//
//        initRenderTargets();
//
//        initLwjglListeners();
//        glEnable(GL_DEPTH_TEST);
//    }
//
//    private void basicInit() throws IOException
//    {
//
//        windowMainAudio.initWidth = 800;
//        windowMainAudio.initHeight = 800;
//
//        GLFWErrorCallback.createPrint(System.err).set();
//        if ( !glfwInit() )
//            throw new IllegalStateException("Unable to initialize GLFW");
//
//        glfwDefaultWindowHints(); // optional, the current window hints are already the default
//        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
//        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable
//
//        // Free the window callbacks and destroy the window
//        try{
//            glfwFreeCallbacks(windowMainAudio.window);
//            glfwDestroyWindow(windowMainAudio.window);
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//
//        windowMainAudio.window = glfwCreateWindow(windowMainAudio.initWidth, windowMainAudio.initHeight, "Audio visualisation.", NULL, NULL);
//        if ( windowMainAudio.window == NULL )
//            throw new RuntimeException("Failed to create the GLFW window");
//
//        // init size of window because of start rendering on linux
//        windowMainAudio.width = windowMainAudio.initWidth;
//        windowMainAudio.height = windowMainAudio.initHeight;
//
//        windowMainAudio.proj = new Mat4PerspRH(Math.PI / 4, windowMainAudio.height / (double) windowMainAudio.width, 1, 100.0);
//
//        glfwSetKeyCallback(windowMainAudio.window, (window, key, scancode, action, mods) -> {
//            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE ) {
//                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
//                System.exit(0);
//            }
//        });
//
//        glfwSetFramebufferSizeCallback(windowMainAudio.window, new GLFWFramebufferSizeCallback() {
//            @Override
//            public void invoke(long window, int width, int height) {
//                if (width > 0 && height > 0 &&
//                        (windowMainAudio.width != width || windowMainAudio.height != height)) {
//                    windowMainAudio.width = width;
//                    windowMainAudio.height = height;
//                    windowMainAudio.proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 1, 100.0);
//                }
//            }
//        });
//        try ( MemoryStack stack = stackPush() ) {
//            IntBuffer pWidth = stack.mallocInt(1); // int*
//            IntBuffer pHeight = stack.mallocInt(1); // int*
//
//            // Get the window size passed to glfwCreateWindow
//            glfwGetWindowSize(windowMainAudio.window, pWidth, pHeight);
//
//            // Get the resolution of the primary monitor
//            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
//
//            // Center the window
//            glfwSetWindowPos(
//                    windowMainAudio.window,
//                    (vidmode.width() - pWidth.get(0)) / 2,
//                    (vidmode.height() - pHeight.get(0)) / 2
//            );
//        }
//        glfwMakeContextCurrent(windowMainAudio.window);
//        glfwSwapInterval(1);
//        glfwShowWindow(windowMainAudio.window);
//        GL.createCapabilities();
//        OGLUtils.printOGLparameters();
//        OGLUtils.printLWJLparameters();
//        OGLUtils.printJAVAparameters();
//
//        // Set the clear color
//        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
//    }
//
//    private void initShaders()
//    {
//
//        windowMainAudio.shaderProgram = ShaderUtils.loadProgram("/shaders/audio/shader.vert",
//                "/shaders/audio/shader.frag",
//                null,null,null,null);
//
//        // Shader program set
//        glUseProgram(windowMainAudio.shaderProgram);
//    }
//
//    private void initLocationVars()
//    {
//        windowMainAudio.locTime = glGetUniformLocation(windowMainAudio.shaderProgram, "time");
//
//        // ** AO
////        windowMainAudio.locSceneView = glGetUniformLocation(windowMainAudio.shaderProgram, "viewMatrix");
////        windowMainAudio.locSceneProjection = glGetUniformLocation(windowMainAudio.shaderProgram, "projectionMatrix");
////        windowMainAudio.locTime = glGetUniformLocation(windowMainAudio.shaderProgram, "time");
//
//        windowMainAudio.locColor = glGetUniformLocation(windowMainAudio.shaderProgram, "inColor");
//        windowMainAudio.locMat = glGetUniformLocation(windowMainAudio.shaderProgram, "mat");
//    }
//
//    private void initSceneVars()
//    {
//        windowMainAudio.red = 1f;
//        windowMainAudio.green = 1f;
//        windowMainAudio.blue = 1f;
//        windowMainAudio.gridWidth = 100;
//        windowMainAudio.gridHeight = 100;
//    }
//
//    private void initScene() throws IOException
//    {
//        windowMainAudio.buffers = GridFactory.makeGrid(windowMainAudio.gridWidth, windowMainAudio.gridHeight);
//        windowMainAudio.proj = new Mat4PerspRH(Math.PI / 4, windowMainAudio.height / (double) windowMainAudio.width, 1, 100.0);
//        windowMainAudio.cam = windowMainAudio.cam.withPosition(new Vec3D(5, 5, 2.5))
//                .withAzimuth(Math.PI * 1.25)
//                .withZenith(Math.PI * -0.125);
////        windowMain.lightCam = windowMain.lightCam.withPosition(new Vec3D(windowMain.lightPosVec[0], windowMain.lightPosVec[2], windowMain.lightPosVec[2]))
////                .withAzimuth(Math.PI * 1.25)
////                .withZenith(Math.PI * -0.125);
//    }
//
//    private void initShaderVars()
//    {
//        glPointSize(windowMainAudio.pointSize);
//        float[] colors = {windowMainAudio.red, windowMainAudio.green, windowMainAudio.blue};
//        glUniform3fv(windowMainAudio.locColor, colors);
//    }
//
//    private void initRenderTargets()
//    {
//        final int size = 1024;
//        windowMainAudio.shaderRT = new OGLRenderTarget(size, size, 4);
//    }
//
//    private void initLwjglListeners()
//    {
//        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
//        glfwSetKeyCallback(windowMainAudio.window, (window, key, scancode, action, mods) -> {
//            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
//                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
//            if (action == GLFW_PRESS || action == GLFW_REPEAT){
//                switch (key) {
//                    case GLFW_KEY_W:
//                        windowMainAudio.cam = windowMainAudio.cam.forward(windowMainAudio.CAM_MOVE);
//                        break;
//                    case GLFW_KEY_D:
//                        windowMainAudio.cam = windowMainAudio.cam.right(windowMainAudio.CAM_MOVE);
//                        break;
//                    case GLFW_KEY_S:
//                        windowMainAudio.cam = windowMainAudio.cam.backward(windowMainAudio.CAM_MOVE);
//                        break;
//                    case GLFW_KEY_A:
//                        windowMainAudio.cam = windowMainAudio.cam.left(windowMainAudio.CAM_MOVE);
//                        break;
//                    case GLFW_KEY_LEFT_CONTROL:
//                        windowMainAudio.cam = windowMainAudio.cam.down(1);
//                        break;
//                    case GLFW_KEY_LEFT_SHIFT:
//                        windowMainAudio.cam = windowMainAudio.cam.up(1);
//                        break;
//                    case GLFW_KEY_SPACE:
//                        windowMainAudio.cam = windowMainAudio.cam.withFirstPerson(!windowMainAudio.cam.getFirstPerson());
//                        break;
//                    case GLFW_KEY_TAB:
//                        windowMainAudio.cam = windowMainAudio.cam.withPosition(new Vec3D(5, 5, 2.5))
//                                .withAzimuth(Math.PI * 1.25)
//                                .withZenith(Math.PI * -0.125);
//                        break;
//                }
//            }
//        });
//
//        glfwSetCursorPosCallback(windowMainAudio.window, new GLFWCursorPosCallback() {
//            @Override
//            public void invoke(long window, double x, double y) {
//                if (windowMainAudio.mouseButton1) {
//                        windowMainAudio.cam = windowMainAudio.cam.addAzimuth(Math.PI * (windowMainAudio.ox - x) / windowMainAudio.width)
//                                .addZenith(Math.PI * (windowMainAudio.oy - y) / windowMainAudio.width);
//                        windowMainAudio.ox = x;
//                        windowMainAudio.oy = y;
//                }
//            }
//        });
//
//        glfwSetMouseButtonCallback(windowMainAudio.window, new GLFWMouseButtonCallback() {
//            @Override
//            public void invoke(long window, int button, int action, int mods) {
//                windowMainAudio.mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;
//                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
//                    windowMainAudio.mouseButton1 = true;
//                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
//                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
//                    glfwGetCursorPos(window, xBuffer, yBuffer);
//                    windowMainAudio.ox = xBuffer.get(0);
//                    windowMainAudio.oy = yBuffer.get(0);
//                }
//                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
//                    windowMainAudio.mouseButton1 = false;
//                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
//                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
//                    glfwGetCursorPos(window, xBuffer, yBuffer);
//                    double x = xBuffer.get(0);
//                    double y = yBuffer.get(0);
//                    windowMainAudio.cam = windowMainAudio.cam.addAzimuth(Math.PI * (windowMainAudio.ox - x) / windowMainAudio.width)
//                            .addZenith(Math.PI * (windowMainAudio.oy - y) / windowMainAudio.width);
//                    windowMainAudio.ox = x;
//                    windowMainAudio.oy = y;
//                }
//            }
//
//        });
//    }
//
//
//
//
//}
