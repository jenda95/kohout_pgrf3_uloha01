/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.window;

import app.util.GridFactory;
import app.util.RandomTextureGenerator;
import lwjglutils.*;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;
import transforms.Mat3RotZ;
import transforms.Mat4PerspRH;
import transforms.Vec3D;

import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.glfw.GLFW.glfwSetFramebufferSizeCallback;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glPointSize;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

public class InitWindowUtils {
    private WindowMain windowMain;
    private RenderUtils renderUtils;

    public InitWindowUtils(WindowMain windowMain)
    {
        this.windowMain = windowMain;
        this.renderUtils = new RenderUtils(this.windowMain);
    }

    public void completeInit() throws IOException
    {
        basicInit();
        initShaders();
        initLocationVars();
        initSceneVars();
        initScene();
        initShaderVars();

        // *** AO
        initRenderTargets();

        initLwjglListeners();
        glEnable(GL_DEPTH_TEST);
    }

    private void basicInit() throws IOException
    {

        windowMain.initWidth = 800;
        windowMain.initHeight = 800;

        GLFWErrorCallback.createPrint(System.err).set();
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        // Free the window callbacks and destroy the window
        try{
            glfwFreeCallbacks(windowMain.window);
            glfwDestroyWindow(windowMain.window);
        } catch (Exception e) {
            System.out.println(e);
        }

        windowMain.window = glfwCreateWindow(windowMain.initWidth, windowMain.initHeight, "Hello World!", NULL, NULL);
        if ( windowMain.window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // init size of window because of start rendering on linux
        windowMain.width = windowMain.initWidth;
        windowMain.height = windowMain.initHeight;

        windowMain.proj = new Mat4PerspRH(Math.PI / 4, windowMain.height / (double) windowMain.width, 1, 100.0);

        glfwSetKeyCallback(windowMain.window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE ) {
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
                System.exit(0);
            }
        });

        glfwSetFramebufferSizeCallback(windowMain.window, new GLFWFramebufferSizeCallback() {
            @Override
            public void invoke(long window, int width, int height) {
                if (width > 0 && height > 0 &&
                        (windowMain.width != width || windowMain.height != height)) {
                    windowMain.width = width;
                    windowMain.height = height;
                    windowMain.proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 1, 100.0);
                }
            }
        });
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(windowMain.window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    windowMain.window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        }
        glfwMakeContextCurrent(windowMain.window);
        glfwSwapInterval(1);
        glfwShowWindow(windowMain.window);
        GL.createCapabilities();
        OGLUtils.printOGLparameters();
        OGLUtils.printLWJLparameters();
        OGLUtils.printJAVAparameters();

        // Set the clear color
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    }

    private void initShaders()
    {
//        windowMain.shaderProgram = ShaderUtils.loadProgram("/shaders/old/uniform.vert",
//                "/shaders/old/uniform.frag",
//                null,null,null,null);
//
        windowMain.shaderProgram2 = ShaderUtils.loadProgram("/shaders/old/object.vert",
                "/shaders/old/object.frag",
                null,null,null,null);

        // ** AO
        windowMain.sceneShader = ShaderUtils.loadProgram("/shaders/scene");
        windowMain.ssaoShader = ShaderUtils.loadProgram("/shaders/ssao");
        windowMain.blurShader = ShaderUtils.loadProgram("/shaders/blur");
        windowMain.shadeShader = ShaderUtils.loadProgram("/shaders/shade");

        // Shader program set
        glUseProgram(windowMain.shaderProgram);
    }

    private void initLocationVars()
    {
        windowMain.locSpotlight = glGetUniformLocation(windowMain.shaderProgram, "spotlight");
        windowMain.locSpotlightVec = glGetUniformLocation(windowMain.shaderProgram, "spotlightSource");
        windowMain.locTime = glGetUniformLocation(windowMain.shaderProgram, "time");

        // ** AO
        windowMain.locSceneView = glGetUniformLocation(windowMain.sceneShader, "viewMatrix");
        windowMain.locSceneProjection = glGetUniformLocation(windowMain.sceneShader, "projectionMatrix");
        windowMain.locTime = glGetUniformLocation(windowMain.sceneShader, "time");
        windowMain.locFunction = glGetUniformLocation(windowMain.sceneShader, "function");
        windowMain.locRotate = glGetUniformLocation(windowMain.sceneShader, "rotate");
        windowMain.locColor = glGetUniformLocation(windowMain.sceneShader, "sampleColor");
        windowMain.locSurfaceMode = glGetUniformLocation(windowMain.sceneShader, "surfaceMode");
        windowMain.locSsaoProjection = glGetUniformLocation(windowMain.ssaoShader, "projectionMatrix");
        windowMain.locShadeView = glGetUniformLocation(windowMain.shadeShader, "view");
        windowMain.ambientIntensityLoc = glGetUniformLocation(windowMain.shadeShader, "ambientIntensity");
        windowMain.diffuseIntensityLoc = glGetUniformLocation(windowMain.shadeShader, "diffuseIntensity");
        windowMain.specularIntensityLoc = glGetUniformLocation(windowMain.shadeShader, "specularIntensity");
        windowMain.locLightPosVec = glGetUniformLocation(windowMain.shadeShader, "lightPosVec");
        windowMain.locAO = glGetUniformLocation(windowMain.shadeShader, "ao");
        windowMain.locBlur = glGetUniformLocation(windowMain.blurShader, "blurIntensity");
    }

    private void initScene() throws IOException
    {
        windowMain.buffers = GridFactory.makeGrid(windowMain.gridWidth, windowMain.gridHeight);
        windowMain.quad = GridFactory.getQuad();
        windowMain.viewer = new OGLTexture2D.Viewer();
        windowMain.proj = new Mat4PerspRH(Math.PI / 4, windowMain.height / (double) windowMain.width, 1, 100.0);
        windowMain.model = new OGLModelOBJ("/objects/ducky.obj");
        renderUtils.createTexture(windowMain.textureName);
        windowMain.cam = windowMain.cam.withPosition(new Vec3D(5, 5, 2.5))
                .withAzimuth(Math.PI * 1.25)
                .withZenith(Math.PI * -0.125);
//        windowMain.lightCam = windowMain.lightCam.withPosition(new Vec3D(windowMain.lightPosVec[0], windowMain.lightPosVec[2], windowMain.lightPosVec[2]))
//                .withAzimuth(Math.PI * 1.25)
//                .withZenith(Math.PI * -0.125);
    }

    private void initSceneVars()
    {
        windowMain.red = 1f;
        windowMain.green = 1f;
        windowMain.blue = 1f;
        windowMain.gridWidth = 100;
        windowMain.gridHeight = 100;
        windowMain.renderMode = 4;
        windowMain.functionMode = 2;
        windowMain.surfaceMode = 0;
        windowMain.textureName = "2k_earth_daymap.jpg";

        windowMain.ambientIntensity = 0.3f;
        windowMain.diffuseIntensity = 0.5f;
        windowMain.specularIntensity = 0.8f;

        windowMain.mainMode = 0;
        windowMain.mainModeChanged = false;

        windowMain.ao = true;
        windowMain.blur = 16;

        try {
            windowMain.textureSample = new OGLTexture2D("./textures/"+windowMain.textureName);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initShaderVars()
    {
        glPointSize(windowMain.pointSize);
        glUniform3f(windowMain.locSpotlightVec,
                (float) windowMain.spotlightVec.getX(),
                (float)windowMain.spotlightVec.getY(),
                (float)windowMain.spotlightVec.getZ());
        glUniform1i(windowMain.locFunction, windowMain.functionMode);
        float[] colors = {windowMain.red, windowMain.green, windowMain.blue};
        glUniform3fv(windowMain.locColor, colors);
        glUniform1i(windowMain.locSurfaceMode, windowMain.surfaceMode);

    }

    private void initRenderTargets()
    {
        final int size = 1024;
        windowMain.sceneRT = new OGLRenderTarget(size, size, 4);
        windowMain.ssaoRT = new OGLRenderTarget(size, size);
        windowMain.blurRT = new OGLRenderTarget(size, size);
        windowMain.randomTexture = RandomTextureGenerator.getTexture();
        final int noiseWidth = 4;
        windowMain.noiseTexture = RandomTextureGenerator.getNoiseTexture(noiseWidth);
    }

    private void initLwjglListeners()
    {
        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(windowMain.window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            if (action == GLFW_PRESS || action == GLFW_REPEAT){
                switch (key) {
                    case GLFW_KEY_W:
                        windowMain.cam = windowMain.cam.forward(windowMain.CAM_MOVE);
                        break;
                    case GLFW_KEY_D:
                        windowMain.cam = windowMain.cam.right(windowMain.CAM_MOVE);
                        break;
                    case GLFW_KEY_S:
                        windowMain.cam = windowMain.cam.backward(windowMain.CAM_MOVE);
                        break;
                    case GLFW_KEY_A:
                        windowMain.cam = windowMain.cam.left(windowMain.CAM_MOVE);
                        break;
                    case GLFW_KEY_LEFT_CONTROL:
                        windowMain.cam = windowMain.cam.down(1);
                        break;
                    case GLFW_KEY_LEFT_SHIFT:
                        windowMain.cam = windowMain.cam.up(1);
                        break;
                    case GLFW_KEY_SPACE:
                        windowMain.cam = windowMain.cam.withFirstPerson(!windowMain.cam.getFirstPerson());
                        break;
                    case GLFW_KEY_TAB:
                        windowMain.cam = windowMain.cam.withPosition(new Vec3D(5, 5, 2.5))
                                .withAzimuth(Math.PI * 1.25)
                                .withZenith(Math.PI * -0.125);
                        break;
                    case GLFW_KEY_CAPS_LOCK:
                        windowMain.capslock = !windowMain.capslock;
                        break;
                }
            }
        });

        glfwSetCursorPosCallback(windowMain.window, new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                if (windowMain.mouseButton1) {
                    if(!windowMain.capslock){
                        windowMain.cam = windowMain.cam.addAzimuth(Math.PI * (windowMain.ox - x) / windowMain.width)
                                .addZenith(Math.PI * (windowMain.oy - y) / windowMain.width);
                        windowMain.ox = x;
                        windowMain.oy = y;
                    } else {
                        windowMain.lightX = (x-windowMain.lightOx)/windowMain.width;
                        windowMain.lightZ = (windowMain.lightOy-y)/windowMain.height;
                        windowMain.spotlightVec = windowMain.spotlightVec.add(new Vec3D(0,0,20*windowMain.lightZ));
                        windowMain.spotlightVec = windowMain.spotlightVec.mul(new Mat3RotZ(windowMain.lightX*2*Math.PI));
                        windowMain.lightOx = x;
                        windowMain.lightOy = y;
                        windowMain.lightPosVec[0] = (float) windowMain.spotlightVec.getX();
                        windowMain.lightPosVec[1] = (float) windowMain.spotlightVec.getY();
                        windowMain.lightPosVec[2] = (float) windowMain.spotlightVec.getZ();
                    }
                }
            }
        });

        glfwSetMouseButtonCallback(windowMain.window, new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                windowMain.mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;
                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
                    windowMain.mouseButton1 = true;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    if(!windowMain.capslock){
                        windowMain.ox = xBuffer.get(0);
                        windowMain.oy = yBuffer.get(0);
                    } else {
                        windowMain.lightOx = xBuffer.get(0);
                        windowMain.lightOy = yBuffer.get(0);
                    }
                }

                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
                    windowMain.mouseButton1 = false;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    double x = xBuffer.get(0);
                    double y = yBuffer.get(0);
                    if(!windowMain.capslock){
                        windowMain.cam = windowMain.cam.addAzimuth(Math.PI * (windowMain.ox - x) / windowMain.width)
                                .addZenith(Math.PI * (windowMain.oy - y) / windowMain.width);
                        windowMain.ox = x;
                        windowMain.oy = y;
                    } else {
                        windowMain.lightX = (x-windowMain.lightOx)/windowMain.width;
                        windowMain.lightZ = (windowMain.lightOy-y)/windowMain.height;
                        windowMain.spotlightVec = windowMain.spotlightVec.add(new Vec3D(0,0,20*windowMain.lightZ*2));
                        windowMain.spotlightVec = windowMain.spotlightVec.mul(new Mat3RotZ(windowMain.lightX*2*Math.PI));
                        windowMain.lightOx = x;
                        windowMain.lightOy = y;
                    }
                }
            }

        });
    }

}
