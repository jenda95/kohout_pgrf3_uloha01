/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.window;

import app.audio.Audio;
import app.util.GridFactory;
import com.github.psambit9791.jdsp.io.Wav;
import com.github.psambit9791.wavfile.WavFileException;
import lwjglutils.*;
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import transforms.*;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.Calendar;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class WindowMainAudio {

    int width, height;

    // The window handle
    private long window;

    OGLBuffers buffers;

    int shaderProgram, locTime, locMat, locFunction, locAudioData;

    Camera cam = new Camera();
    Mat4 proj = new Mat4PerspRH(Math.PI / 4, 1, 1, 10.0);

    float time = 0;

    protected OGLTexture2D.Viewer viewer;
    protected OGLRenderTarget kickRT, snareRT, hihatRT;

    // grid params
    private int gridWidth;
    private int gridHeight;
    private boolean gridChanged;
    private int renderMode;
    private boolean renderModeChanged;
    private int pointSize;

    // Controls params
    private boolean mouseButton1 = false;
    private double ox, oy;

    private int functionMode;
    private boolean functionModeChanged;

    Audio audio = new Audio();

    private String pathToRecord = "";
    private String pathToKick = "";
    private String pathToSnare = "";
    private String pathToHihat = "";

    private boolean record = true;
    private boolean kick = true;
    private boolean snare = true;
    private boolean hihat = true;

    private double[][] kickData;
    private double[] kickData1D;

    private int counterKick = 4;

    public WindowMainAudio() throws IOException, UnsupportedAudioFileException {
    }

    private void init() throws InterruptedException, IOException, WavFileException {
        width = 800;
        height = 800;
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure GLFW
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

        // Create the window
        window = glfwCreateWindow(width, height, "Hello World!", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
        });

        glfwSetFramebufferSizeCallback(window, new GLFWFramebufferSizeCallback() {
            @Override
            public void invoke(long window, int width, int height) {
                if (width > 0 && height > 0 &&
                        (WindowMainAudio.this.width != width || WindowMainAudio.this.height != height)) {
                    WindowMainAudio.this.width = width;
                    WindowMainAudio.this.height = height;
                }
            }
        });

        this.initLwjglListeners();

        // Get the thread stack and push a new frame
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight);

            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);

        // Make the window visible
        glfwShowWindow(window);

        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        OGLUtils.printOGLparameters();
        OGLUtils.printLWJLparameters();
        OGLUtils.printJAVAparameters();

        // Set the clear color
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        this.gridWidth = 10;
        this.gridHeight = 10;
        this.buffers = GridFactory.makeGrid(this.gridWidth, this.gridHeight);
        glPointSize(this.pointSize);
        this.renderMode = 3;

        System.out.println(buffers);

        shaderProgram = ShaderUtils.loadProgram("/shaders/audio/shader.vert",
                "/shaders/audio/shader.frag",
                null,null,null,null);

        // Shader program set
        glUseProgram(this.shaderProgram);

        // Matice kamery
        locMat = glGetUniformLocation(shaderProgram, "mat");

        // Inicializace funkce
        this.functionMode = 0;
        locFunction = glGetUniformLocation(shaderProgram, "function");
        glUniform1i(locFunction, this.functionMode);

        // Kamera
        cam = cam.withPosition(new Vec3D(3, 3, 2.5))
                .withAzimuth(Math.PI * 1.25)
                .withZenith(Math.PI * -0.125);

        // internal OpenGL ID of a shader uniform (constant during one draw call
        // - constant value for all processed vertices or pixels) variable

        locTime = glGetUniformLocation(shaderProgram, "time");
//        viewer = new OGLTexture2D.Viewer();
//        kickRT = new OGLRenderTarget(1024,1024, 1);


        glfwSetWindowSize(window, width, height);

    }

//    private void renderKick()
//    {
//        kickRT.bind();
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
////            time += 0.1;
//        time = (float)kickData1D[counterKick];
////            System.out.println(kickData1D[counter]);
//
//        counterKick++;
////            glUniform1f(locTime, time);
//        glUniform1f(locTime, time);
//        glUniformMatrix4fv(locMat, false,
//                ToFloatArray.convert(cam.getViewMatrix().mul(proj).mul(new Mat4Scale((double)width / height, 1, 1))));
//
//        buffers.draw(this.renderMode, shaderProgram);
//
//    }

    private void renderFinal() {}

    private void loop() throws InterruptedException {

        Calendar calendar1 = Calendar.getInstance();
        java.util.Date start = calendar1.getTime();
//        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(start.getTime());

        System.out.println(start.getTime());

        glViewport(0, 0, width, height);
        glUseProgram(shaderProgram);
        glPointSize(pointSize);

        int lastFPS = 0;

        Calendar calendarWhile1 = Calendar.getInstance();
        java.util.Date whileStart = calendarWhile1.getTime();

        Calendar calendarWhile2 = Calendar.getInstance();
        java.util.Date whileEnd = calendarWhile2.getTime();

        glEnable(GL_DEPTH_TEST);

        while ( !glfwWindowShouldClose(window)) {
            glUniform1i(locFunction, this.functionMode);
            if(this.functionMode == 0) {
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
                time = 0;
                glUniform1f(locTime, time);
                glUniformMatrix4fv(locMat, false,
                        ToFloatArray.convert(cam.getViewMatrix().mul(proj).mul(new Mat4Scale((double)width / height, 1, 1))));
                buffers.draw(this.renderMode, shaderProgram);
                glfwSwapBuffers(window); // swap the color buffers
                glfwPollEvents();
            } else if (this.functionMode == 1) {
                if(counterKick >= kickData1D.length) {
                    this.functionMode = 0;
                }
                calendarWhile1 = Calendar.getInstance();
                whileStart = calendarWhile1.getTime();
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

//            time += 0.1;
                time = (float)kickData1D[counterKick];
//            System.out.println(kickData1D[counter]);

                counterKick++;
//            glUniform1f(locTime, time);
                glUniform1f(locTime, time);
                glUniformMatrix4fv(locMat, false,
                        ToFloatArray.convert(cam.getViewMatrix().mul(proj).mul(new Mat4Scale((double)width / height, 1, 1))));

                buffers.draw(this.renderMode, shaderProgram);



//                renderKick();
//                viewer.view(kickRT.getColorTexture(0), -1, 0.5, 0.5);

                glfwSwapBuffers(window); // swap the color buffers
                glfwPollEvents();




                calendarWhile2 = Calendar.getInstance();
                whileEnd = calendarWhile2.getTime();
                long estimatedLoop = whileEnd.getTime()-whileStart.getTime();
                while(estimatedLoop < 23) {
                    Thread.sleep(1);
                    calendarWhile2 = Calendar.getInstance();
                    whileEnd = calendarWhile2.getTime();
                    estimatedLoop = whileEnd.getTime()-whileStart.getTime();
                }
//                if( estimatedLoop < 16) {
//                    Thread.sleep(21);
//                }
            }

        }
        Calendar calendar2 = Calendar.getInstance();
        java.util.Date stop = calendar2.getTime();
        System.out.println("Estimated time(ms): "+(stop.getTime()-start.getTime()));
        System.out.println("Estimated time(s): "+(stop.getTime()-start.getTime())/1000);

//        glfwSwapBuffers(window); // swap the color buffers
//        glfwPollEvents();
    }

    public void run() {
        try {
            System.out.println("Hello LWJGL " + Version.getVersion() + "!");
            init();

            loop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(window);
            glfwDestroyWindow(window);

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            // Terminate GLFW and free the error callback
            glDeleteProgram(shaderProgram);
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }

    }

    /**
     * Answers for GUI listeners
     */

    public void changeWidth(int width) {
        if(width > 2) {
            this.gridWidth = width;
            gridChanged = true;
        }
    }

    public void changeHeight(int height) {
        if(width > 2) {
            this.gridHeight = height;
            gridChanged = true;
        }
    }

    public void changePointSize(int pointSize) {
        this.pointSize = pointSize;
    }

    public void changeRenderMode(String renderMode) {
//        "Points", "Lines", "Triangles"
        System.out.println(renderMode);
        switch(renderMode) {
            case "Points":
                this.renderMode = GL_POINTS;
                break;
            case "Lines":
                this.renderMode = GL_LINES;
                break;
            case "Triangles":
                this.renderMode = GL_TRIANGLES;
                break;
        }
        this.renderModeChanged = true;
    }

    public void changeFunctionMode(String functionMode) {
//        "Koule", "Vejir", "Vaza", "Valec", "Plocha", "Plocha pohyb"
        System.out.println(functionMode);
        switch(functionMode) {
            case "Koule":
                this.functionMode = 2;
                break;
            case "Vejir":
                this.functionMode = 3;
                break;
            case "Vaza":
                this.functionMode = 4;
                break;
            case "Valec":
                this.functionMode = 5;
                break;
            case "Plocha":
                this.functionMode = 0;
                break;
            case "Plocha pohyb":
                this.functionMode = 1;
                break;
        }
        this.functionModeChanged = true;
    }

    private void initLwjglListeners() {
        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
                glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
            if (action == GLFW_PRESS || action == GLFW_REPEAT){
                switch (key) {
                    case GLFW_KEY_W:
                        cam = cam.forward(1);
                        break;
                    case GLFW_KEY_D:
                        cam = cam.right(1);
                        break;
                    case GLFW_KEY_S:
                        cam = cam.backward(1);
                        break;
                    case GLFW_KEY_A:
                        cam = cam.left(1);
                        break;
                    case GLFW_KEY_LEFT_CONTROL:
                        cam = cam.down(1);
                        break;
                    case GLFW_KEY_LEFT_SHIFT:
                        cam = cam.up(1);
                        break;
                    case GLFW_KEY_SPACE:
                        cam = cam.withFirstPerson(!cam.getFirstPerson());
                        break;
                    case GLFW_KEY_R:
                        cam = cam.mulRadius(0.9f);
                        break;
                    case GLFW_KEY_F:
                        cam = cam.mulRadius(1.1f);
                        break;
                }
            }
        });

        glfwSetCursorPosCallback(window, new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                if (mouseButton1) {
                    cam = cam.addAzimuth((double) Math.PI * (ox - x) / width)
                            .addZenith((double) Math.PI * (oy - y) / width);
                    ox = x;
                    oy = y;

                }
            }
        });

        glfwSetMouseButtonCallback(window, new GLFWMouseButtonCallback () {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS){
                    mouseButton1 = true;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    ox = xBuffer.get(0);
                    oy = yBuffer.get(0);
                }

                if (button==GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE){
                    mouseButton1 = false;
                    DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                    DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                    glfwGetCursorPos(window, xBuffer, yBuffer);
                    double x = xBuffer.get(0);
                    double y = yBuffer.get(0);
                    cam = cam.addAzimuth((double) Math.PI * (ox - x) / width)
                            .addZenith((double) Math.PI * (oy - y) / width);
                    ox = x;
                    oy = y;
                }
            }

        });
    }

    public void setPathToRecord(String pathToRecord)
    {
        this.pathToRecord = pathToRecord;
    }

    public void setPathToKick(String pathToKick)
    {
        this.pathToKick = pathToKick;
    }

    public void setPathToSnare(String pathToSnare)
    {
        this.pathToSnare = pathToSnare;
    }

    public void setPathToHihat(String pathToHihat)
    {
        this.pathToHihat = pathToHihat;
    }

    public void setRecord(boolean record)
    {
        this.record = record;
    }

    public void setKick(boolean kick)
    {
        this.kick = kick;
    }

    public void setSnare(boolean snare)
    {
        this.snare = snare;
    }

    public void setHihat(boolean hihat)
    {
        this.hihat = hihat;
    }

    public void setFunctionMode(int functionMode)
    {
        this.functionMode = functionMode;
    }

    public void initCounters()
    {
        this.counterKick = 4;
    }

    public void initKickSample() throws IOException, WavFileException {
        kickData = audio.readWav(pathToKick);
        kickData1D = audio.downsampleAudio(kickData);
        kickData1D = audio.downsampleAudio(kickData1D, 10, 44100);
        System.out.println("Kick data length: " + kickData1D.length);
    }
}




















