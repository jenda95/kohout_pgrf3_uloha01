/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.window;

import app.util.UtilFunctions;
import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBImage;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.stbi_image_free;

public class RenderUtils {
    WindowMain windowMain;

    RenderUtils(WindowMain windowMain)
    {
        this.windowMain = windowMain;
    }

    void createTexture(String textureName) throws IOException {
        IntBuffer width = BufferUtils.createIntBuffer(1);
        IntBuffer height = BufferUtils.createIntBuffer(1);
        IntBuffer components = BufferUtils.createIntBuffer(1);
        ByteBuffer data = STBImage.stbi_load_from_memory(UtilFunctions.ioResourceToByteBuffer("textures/"+textureName, 1024), width, height, components, 4);
        windowMain.id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, windowMain.id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width.get(), height.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        stbi_image_free(data);
    }

}
