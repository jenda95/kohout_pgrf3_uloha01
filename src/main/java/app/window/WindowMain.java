/*
 * Copyright 2014 - 2020
 * Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove
 *
 * This file is part of PGRF_03 project.
 *
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.
 */

package app.window;

import app.util.GridFactory;
import lwjglutils.*;
import org.lwjgl.*;

import transforms.*;

import java.io.IOException;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL33.*;

public class WindowMain {
    private RenderUtils renderUtils;


    int width, height;

    int initWidth, initHeight;

    // The window handle
    protected long window;

    OGLBuffers buffers;
    OGLModelOBJ model, model2;

    int shaderProgram, shaderProgram2, locTime, locMat, locFunction, locSurfaceMode, locColor, locMv, locProj, locEyePos, locSpotlight, locSpotlightVec;

    Camera cam = new Camera();
    Mat4 proj;
    float time = 0;

    // grid params
    protected int gridWidth;
    protected int gridHeight;
    protected boolean gridChanged;
    protected int renderMode;
    protected boolean renderModeChanged;
    protected int pointSize;
    protected boolean textureChanged;
    protected String textureName;

    // Controls params
    protected boolean mouseButton1 = false;
    protected double ox, oy;

    protected int functionMode;
    protected boolean functionModeChanged;

    protected int surfaceMode;
    protected boolean surfaceModeChanged;

    protected float red, green, blue;

    protected int perspectiveMode;
    protected boolean perspectiveModeChanged;

    protected boolean capslock = false;
    protected boolean spotlight = false;
    protected Vec3D spotlightVec = new Vec3D(5f,5f,2.5f);;

    protected double lightX, lightOx, lightY, lightOy, lightZ;

    int id;
    protected static double CAM_MOVE = 0.1;

    // ****AO****
    protected int sceneShader, ssaoShader, blurShader, shadeShader;
    protected OGLRenderTarget sceneRT, ssaoRT, blurRT;
    protected final float[] colors = {1f, 1f, 1f};
    protected int locRotate, locSceneView, locSceneProjection, locSsaoProjection, locShadeView;
    protected int ambientIntensityLoc, diffuseIntensityLoc, specularIntensityLoc, locLightPosVec;
    protected boolean rotate = false;
    protected OGLTexture2D textureSample;
    protected OGLTexture2D randomTexture, noiseTexture;
    protected OGLBuffers quad;
    protected OGLTexture2D.Viewer viewer;
    protected int locAO, locBlur;

    protected float ambientIntensity;
    protected float diffuseIntensity;
    protected float specularIntensity;

    protected float[] lightPosVec = {3f, 3f, 10f};

    protected float timeflow = 0.5f;

    protected int mainMode;
    protected boolean mainModeChanged;

    protected boolean ao;
    protected int blur;

    private void init() throws IOException
    {
        this.renderUtils = new RenderUtils(this);
        InitWindowUtils initWindowUtils = new InitWindowUtils(this);
        initWindowUtils.completeInit();
    }

    private void loop() throws IOException, InterruptedException {
        while ( !glfwWindowShouldClose(window) ) {
//            Thread.sleep(500);

            glViewport(0, 0, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

            glUniformMatrix4fv(locMv, false, ToFloatArray.convert(cam.getViewMatrix()));
            glUniformMatrix4fv(locProj, false, ToFloatArray.convert(proj));
            glUniform3f(locEyePos, (float) cam.getPosition().getX(),
                    (float) cam.getPosition().getY(),
                    (float) cam.getPosition().getZ());
            glUniform1i(locSpotlight, spotlight ? 1 : 0);
            glUniform3f(locSpotlightVec, (float) spotlightVec.getX(), (float)spotlightVec.getY(), (float)spotlightVec.getZ());

            setPerspectiveMode();

            if(functionMode == 6 || functionMode == 7) {
                if(functionMode == 6) {
                    if(functionModeChanged) {
                        glUseProgram(this.shaderProgram2);
                        locMat = glGetUniformLocation(shaderProgram2, "mat");
                        model = new OGLModelOBJ("/objects/ducky.obj");
                        this.buffers = model.getBuffers();
                    }
                    glUniformMatrix4fv(locMat, false,
                            ToFloatArray.convert(new Mat4RotX(Math.PI/2).mul(new Mat4Transl(2,0,0).mul(cam.getViewMatrix().mul(proj)))));
                    buffers.draw(model.getTopology(), shaderProgram2);
                    functionModeChanged = false;
                } else {
                    if(functionModeChanged) {
                        glUseProgram(this.shaderProgram2);
                        locMat = glGetUniformLocation(shaderProgram2, "mat");
                        model = new OGLModelOBJ("/objects/ducky.obj");
                        model2 = new OGLModelOBJ("/objects/elephant.obj");
                        functionModeChanged = false;
                    }
                    glUniformMatrix4fv(locMat, false,
                            ToFloatArray.convert(cam.getViewMatrix().mul(proj).mul(new Mat4Scale((double)width / height, 1, 1))));
                    this.buffers = model.getBuffers();
                    buffers.draw(model.getTopology(), shaderProgram2);
                    glUniformMatrix4fv(locMat, false,
                            ToFloatArray.convert(new Mat4Transl(2,0,0).mul(cam.getViewMatrix().mul(proj))));
                    this.buffers = model2.getBuffers();
                    buffers.draw(model2.getTopology(), shaderProgram2);
                }
            } else {
                if(functionModeChanged && mainModeChanged){
                    InitWindowUtils initWindowUtils = new InitWindowUtils(this);
                    initWindowUtils.completeInit();
                    functionModeChanged = false;
                    mainModeChanged = false;
                }
                setGrid();
                setTexture();
                glEnable(GL_DEPTH_TEST);

                renderScene();
                renderSSAO();
                renderBlur();
                renderFinal();

                viewer.view(ssaoRT.getColorTexture(), -1, 0.5, 0.5);
                viewer.view(sceneRT.getColorTexture(0), -1, 0, 0.5);
                viewer.view(sceneRT.getColorTexture(1), -1, -0.5, 0.5);
                viewer.view(sceneRT.getColorTexture(2), -1, -1, 0.5);
                viewer.view(sceneRT.getDepthTexture(), -0.5, -1, 0.5);
                viewer.view(blurRT.getColorTexture(0), 0, -1, 0.5);
            }
// ***************************************************************
// ------------- BEFORE AO, once upon a time in stone age ....
// ***************************************************************
//            if(this.functionMode == 6) {
//            // Mode for objects
//                if(this.functionModeChanged) {
//                    glUseProgram(this.shaderProgram2);
//                    locMat = glGetUniformLocation(shaderProgram2, "mat");
//                    model = new OGLModelOBJ("/objects/ducky.obj");
//                    this.buffers = model.getBuffers();
//                }
//                glUniformMatrix4fv(locMat, false,
//                        ToFloatArray.convert(new Mat4RotX(Math.PI/2).mul(new Mat4Transl(2,0,0).mul(cam.getViewMatrix().mul(proj)))));
//                buffers.draw(model.getTopology(), shaderProgram2);
//                functionModeChanged = false;
//            } else if (this.functionMode == 7) {
//                if(functionModeChanged) {
//                    glUseProgram(this.shaderProgram2);
//                    locMat = glGetUniformLocation(shaderProgram2, "mat");
//                    model = new OGLModelOBJ("/objects/ducky.obj");
//                    model2 = new OGLModelOBJ("/objects/elephant.obj");
//                    functionModeChanged = false;
//                }
//                glUniformMatrix4fv(locMat, false,
//                        ToFloatArray.convert(cam.getViewMatrix().mul(proj).mul(new Mat4Scale((double)width / height, 1, 1))));
//                this.buffers = model.getBuffers();
//                buffers.draw(model.getTopology(), shaderProgram2);
//                glUniformMatrix4fv(locMat, false,
//                        ToFloatArray.convert(new Mat4Transl(2,0,0).mul(cam.getViewMatrix().mul(proj))));
//                this.buffers = model2.getBuffers();
//                buffers.draw(model2.getTopology(), shaderProgram2);
//            }
//            else {
//            // Mode for functions
//                if(this.functionModeChanged) {
//                    glUseProgram(shaderProgram);
//                    this.buffers = GridFactory.makeGrid(this.gridWidth, this.gridHeight);
//                }
//                if (gridChanged) {
//                    this.buffers = GridFactory.makeGrid(this.gridWidth, this.gridHeight);
//                    gridChanged = false;
//                }
//                if (this.surfaceMode == 0 || this.surfaceMode == 3) {
//                    if(textureChanged || surfaceModeChanged) {
//                        renderUtils.createTexture(this.textureName);
//                        glBindTexture(GL_TEXTURE_2D, id);
//                        glUniform1i(locSurfaceMode, this.surfaceMode);
//                        textureChanged = false;
//                        surfaceModeChanged = false;
//                    }
//                } else if (this.surfaceMode == 1) {
//                    if(surfaceModeChanged || colorChanged){
//                        float[] colors = {this.red, this.green, this.blue};
//                        glUniform3fv(locColor, colors);
//                        glUniform1i(locSurfaceMode, this.surfaceMode);
//                        surfaceModeChanged = false;
//                        colorChanged = false;
//                    }
//                } else if (this.surfaceMode == 5) {
//                    if(surfaceModeChanged) {
//                        glUniform1i(locSurfaceMode, this.surfaceMode);
//                        surfaceModeChanged = false;
//                        colorChanged = false;
//                    }
//                } else if (this.surfaceMode == 2) {
//                    if(surfaceModeChanged) {
//                        glUniform1i(locSurfaceMode, this.surfaceMode);
//                        surfaceModeChanged = false;
//                        colorChanged = false;
//                    }
//                }
//
//                if(textureChanged) {
//                    renderUtils.createTexture(this.textureName);
//                    glBindTexture(GL_TEXTURE_2D, id);
//                    textureChanged = false;
//                }
//                glUniform1i(locFunction, this.functionMode);
//                if (renderModeChanged) {
//                    buffers.draw(renderMode, shaderProgram);
//                    this.renderModeChanged = false;
//                }
//                // dodrzovat MVP
//                glUniformMatrix4fv(locMat, false,
//                        ToFloatArray.convert(new Mat4RotX(Math.PI).mul(new Mat4RotY(-Math.PI/7)).mul(cam.getViewMatrix().mul(proj))));
//
//                buffers.draw(this.renderMode, shaderProgram);
//                this.functionModeChanged = false;
//            }

            glfwSwapBuffers(window);
            glfwPollEvents();
        }
    }

    public void run() {
        try {
            System.out.println("Hello LWJGL " + Version.getVersion() + "!");
            init();
            loop();

            // Free the window callbacks and destroy the window
            glfwFreeCallbacks(window);
            glfwDestroyWindow(window);

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            // Terminate GLFW and free the error callback
            glDeleteProgram(shaderProgram);
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }

    }

    private void setPerspectiveMode()
    {
        if(this.perspectiveModeChanged) {
            if(this.perspectiveMode == 1) {
                proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 1, 10.0);
            } else if (this.perspectiveMode == 2) {
                proj = new Mat4OrthoRH(-6, 6, 1, 10.0);
            }
            this.perspectiveModeChanged = false;
        }
    }

    private void setGrid()
    {
        if (gridChanged) {
            this.buffers = GridFactory.makeGrid(this.gridWidth, this.gridHeight);
            gridChanged = false;
        }
    }

    private void setTexture() throws IOException
    {
        if(textureChanged) {
            System.out.println("texture changed");
            try {
                textureSample = new OGLTexture2D("./textures/"+textureName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            textureChanged = false;
        }
    }

    private void renderScene()
    {
        glUseProgram(sceneShader);
        sceneRT.bind();
        glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        time += timeflow;
        glPointSize(pointSize);
        glUniform1i(locFunction, functionMode); //function
        glUniform3fv(locColor,  colors);
        glUniform1i(locSurfaceMode, surfaceMode);
        glUniform1f(locTime, time); // correct shader must be set before this
        glUniform1f(locRotate, rotate ? 1 : 0);
        glUniformMatrix4fv(locSceneView, false, cam.getViewMatrix().floatArray());
        glUniformMatrix4fv(locSceneProjection, false, proj.floatArray());
        textureSample.bind(sceneShader, "textureSample", 0);
        buffers.draw(renderMode, sceneShader);
    }

    private void renderSSAO() {
        glUseProgram(ssaoShader);
        ssaoRT.bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);

        sceneRT.bindColorTexture(ssaoShader, "positionTexture", 0, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        sceneRT.bindColorTexture(ssaoShader, "normalTexture", 1, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        randomTexture.bind(ssaoShader, "randomTexture", 2);
        noiseTexture.bind(ssaoShader, "noiseTexture", 3);
        glUniformMatrix4fv(locSsaoProjection, false, proj.floatArray());
        quad.draw(GL_TRIANGLES, ssaoShader);
    }

    private void renderBlur() {
        glUseProgram(blurShader);
        blurRT.bind();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_DEPTH_TEST);
        glUniform1f(locBlur, (float)blur);

        ssaoRT.bindColorTexture(blurShader, "positionTexture", 0, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        quad.draw(GL_TRIANGLES, blurShader);
    }

    private void renderFinal() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUseProgram(shadeShader);
        glViewport(0, 0, width, height);
        glClearColor(0.1f, 0.1f, 0.1f, 1);

        sceneRT.bindColorTexture(shadeShader, "positionTexture", 0, 0);
        sceneRT.bindColorTexture(shadeShader, "normalTexture", 1, 1);
        sceneRT.bindDepthTexture(shadeShader, "depthTexture", 2);
        //ssaoRT.bindColorTexture(shadeShader, "ssaoTexture", 3, 0);
        blurRT.bindColorTexture(shadeShader, "blurTexture", 3,0);
        sceneRT.bindColorTexture(shadeShader, "imageTexture", 4,3);

        if(spotlight) {
            glUniform1f(ambientIntensityLoc, 0);
            glUniform1f(diffuseIntensityLoc, 0);
            glUniform1f(specularIntensityLoc, 2);
        } else {
            glUniform1f(ambientIntensityLoc, ambientIntensity);
            glUniform1f(diffuseIntensityLoc, diffuseIntensity);
            glUniform1f(specularIntensityLoc, specularIntensity);
        }

        // Blin-phong init
        glUniform3fv(locLightPosVec, lightPosVec);
        if(ao) {
            glUniform1i(locAO, 1);
        }else {
            glUniform1i(locAO, 0);
        }

        glUniformMatrix4fv(locShadeView, false, cam.getViewMatrix().floatArray());
        quad.draw(GL_TRIANGLES, shadeShader);
    }

}


