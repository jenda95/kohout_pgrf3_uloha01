#version 420
in vec2 texCoord;
out vec4 outColor;

layout (binding=0) uniform sampler2D positionTexture;
layout (binding=1) uniform sampler2D normalTexture;
layout (binding=2) uniform sampler2D depthTexture;
layout (binding=3) uniform sampler2D ssaoTexture;
layout (binding=4) uniform sampler2D imageTexture;
layout (binding=5) uniform sampler2D blurTexture;

const float linearAtt = 0.025;
const float quadraticAtt = 0.0025;
const float spotCutOff = 0.05;
const float constantAtt = 0.05;

uniform float ambientIntensity;
uniform float diffuseIntensity;
uniform float specularIntensity;

uniform int ao;

uniform vec3 lightPosVec;

void main()
{
    vec3 ambient = vec3(ambientIntensity);
    vec3 diffuse = vec3(diffuseIntensity);
    vec3 specular = vec3(specularIntensity);

    vec3 lightPosition = lightPosVec;

    vec3 position = texture(positionTexture, texCoord).xyz;
    vec3 normal = normalize(texture(normalTexture, texCoord).xyz);
    float AO = texture(ssaoTexture, texCoord).r;
    float blur = texture(blurTexture, texCoord).r;
    vec4 totalAmbient;
    if(ao==1) {
        totalAmbient = vec4(ambient * AO, 1.0);
    } else {
        totalAmbient = vec4(ambient, 1.0);
    }
    vec3 light = normalize(lightPosition - position);
    float NdotL = max(0, dot(normalize(normal), normalize(light)));
    vec4 totalDiffuse = vec4(NdotL * diffuse, 1.0);
    vec3 hv = normalize(light + lightPosition);
    float NdotH = max(dot(normal, hv),0.0);
    vec4 totalSpecular = vec4(specular * pow(NdotH, 50), 1.0);

    vec4 finalColor = totalAmbient + totalDiffuse + totalSpecular;
    vec4 textureColor = texture(imageTexture, texCoord);

    float distance = length(lightPosition - position);
    float att = 1.0/(constantAtt + (linearAtt * distance) + quadraticAtt * distance * distance);
    float spotEffect = max(dot(normalize(lightPosition), normalize(-light)), 0);
    if (spotEffect < spotCutOff) {
        finalColor = totalAmbient + att * (totalDiffuse + totalSpecular);
    }else {
        finalColor = totalAmbient;
    }
    outColor = finalColor * textureColor + blur-0.7;
}
