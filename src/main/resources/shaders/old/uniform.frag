#version 150
in vec3 vertColor; // input from the previous pipeline stage
in vec2 texCoord;
in vec3 normal;
out vec4 outColor; // output from the fragment shader
uniform sampler2D textureID;
uniform int surfaceMode;
uniform vec3 color1;


in float distance;

// light parameters
in vec3 diffuseTotalVert;
in vec3 specularTotalVert;

in vec3 spotlightDirection;
uniform bool spotlight;

const float spotlightConeAngle = 0.5; //28.65 deg
const vec3 spotlightAngleDirection = vec3(0.0, 0.0, -3.0);

const float constantAttenuation = 0.05;
const float linearAttenuation = 0.025;
const float quadraticAttenuation = 0.0025;

const float ambientLight = 0.3;

const float spotCutOff = 0.05;

in vec3 lightDirection; // Vektor svetla
in vec3 viewDirection; // Pohledovy vektor

void main() {
	vec3 baseColor;
	vec3 nd = normalize(normal); // Normala
	vec3 ld = normalize(lightDirection); // Vektor svetla

	if(surfaceMode == 0) {
		baseColor = texture(textureID, texCoord).rgb;
	} else if (surfaceMode == 1 || surfaceMode == 5) {
		baseColor = vertColor;
	} else if (surfaceMode == 2) {
		baseColor = normalize(normal);
	} else if (surfaceMode == 3) {
		vec3 textureSample = texture(textureID, texCoord).rgb;
		vec3 normalizedColor = normalize(normal);
		baseColor = textureSample + normalizedColor;
	}

	vec3 hv = normalize(lightDirection + viewDirection); // Half vektor
	float NdotL = max(dot(ld, nd), 0); // Skalarni soucin normaly a vektoru ke svetlu
	float NdotH = max(dot(nd, hv), 0.0); // Skalarni soucin normaly a half vektoru
	//light implementation
	vec3 ambientTotal = ambientLight * baseColor;
	vec3 diffuseTotal, specularTotal;
//	if (computeLightInFS) {
//		float NdotL = max(dot(normalize(lightDirection), finalNormal), 0);
//		diffuseTotal = NdotL * diffuseLight * baseColor;
//
//		vec3 halfVector = normalize(lightDirection + viewDirection);
//		float NdotHV = max(dot(finalNormal, halfVector), 0);
//		specularTotal = vec3(pow(NdotHV, 50) * specularLight);
//	} else {
		diffuseTotal = diffuseTotalVert;
		specularTotal = specularTotalVert;
//	}

//	float att = 1.0/(constantAtt + (linearAtt * distance) + quadraticAtt * distance * distance); // Utlum (attenuation)
//	float spotEffect = max(dot(normalize(spotlightDirection), normalize(-ld)), 0); // Efekt baterky

	float attenuation;
	if (spotlight) {
		float theta = acos(dot(normalize(spotlightDirection), normalize(-spotlightAngleDirection)));
		if (theta < spotlightConeAngle) { // není nasvícen
			attenuation = 0.0;
		} else {
			attenuation = 1.0/(constantAttenuation + (linearAttenuation * distance) + quadraticAttenuation * distance * distance);
		}
//		outColor = vec4(ambientTotal, 1.0);
	} else {
		attenuation = 1.0/(constantAttenuation + (linearAttenuation * distance) + quadraticAttenuation * distance * distance);
//		outColor = vec4(ambientTotal + attenuation * (diffuseTotal + specularTotal), 1.0);
	}
//	float attenuation = 1.0/(constantAttenuation + (linearAttenuation * distance) + quadraticAttenuation * distance * distance);
//	float att = 1.0/(constantAtt + (linearAtt * distance) + quadraticAtt * distance * distance); // Utlum (attenuation)
//	outColor = baseColor;
//	if (spotlight && (spotEffect > spotCutOff)) { // Svetelny kuzel
//		outColor = vec4(ambientTotal*20,0.0);
//	}else {
////		outColor = vec4(totalAmbient + att * (totalDiffuse + totalSpecular), 1.0); // Blinn-Phonguv zobrazovaci model (S utlumem)
//		outColor = vec4(ambientTotal + attenuation * (diffuseTotal + specularTotal), 1.0);
//	}

	outColor = vec4(ambientTotal + attenuation * (diffuseTotal + specularTotal), 1.0);

//	outColor = texture(textureID, texCoord);
}
