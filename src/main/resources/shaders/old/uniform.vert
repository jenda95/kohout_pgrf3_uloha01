#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
uniform int function;
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat;
const float PI = 3.1415;

uniform int surfaceMode;
uniform vec3 color1;
//flat out int surfaceModeOut;

in vec3 inNormal;
in vec2 inTextureCoordinates;

out vec2 texCoord;
out vec3 normal;

// light params
uniform mat4 mMv;

out float distance;

out vec3 diffuseTotalVert;
out vec3 specularTotalVert;

const float diffuseLight = 0.5;
const float specularLight = 0.3;

out vec3 lightDirection;
out vec3 spotlightDirection;

uniform vec3 eyePos;
uniform bool spotlight;

out vec3 viewDirection;

vec3 lightSource = vec3(-2, 10, 15);
//vec3 spotlightSource = vec3(-2, 2, 15);
uniform vec3 spotlightSource;
//vec3 spotLS = spotlighSource;

vec3 getSpherical(vec2 position) {
	float azimuth = position.x * 2.0 * PI;
	float zenith = position.y * PI - PI/2.0;
	float r;
//	int function = 2;
	if (function == 2) {
		r = 1;
		azimuth += time / 20;
	} else {
		r = cos(4 * azimuth);
	}

	vec3 positionWithZ;
	positionWithZ.x= cos(azimuth) * cos(zenith);
	positionWithZ.y= r * sin(azimuth) * cos(zenith);
	positionWithZ.z= r * sin(zenith);

	return positionWithZ;
}

vec3 getCartesian(vec2 position) {
	if (function == 0) {
		return 1.5 * vec3(position.x, position.y, 0.8);
	} else {
		return 1.3 * vec3(position.x * 2 - 1, position.y * 2 - 1, 0.3 * sin(position.x + position.y + time / 4) + 0.7);
	}
}

vec3 getCylindrical(vec2 position) {
	float s = position.x * PI * 2.0;
	float t = position.y * PI * 2.0;
	float r;

	if (function == 4) {
		r = 2.0 + cos(2.0 * t);
	} else {
		r = 2.0 + cos(2.0 * t * (sin(time/8))) * sin(s);
	}

	vec3 positionWithZ;
	positionWithZ.x = cos(s) * r;
	positionWithZ.y = sin(s) * r;
	positionWithZ.z = t - 2;
	return positionWithZ / 4.0;
}

vec3 getFinalPosition(vec2 position) {
	vec3 finalPosition;
	if (function < 2) {
		finalPosition = getCartesian(position);
	} else if (function < 4) {
		finalPosition = getSpherical(position);
	} else {
		finalPosition = getCylindrical(position);
	}
	return finalPosition;
}

vec3 getNormal(vec2 position) {
	const float delta = 0.01;
	vec3 du = getFinalPosition(vec2(position.x + delta, position.y)) - getFinalPosition(vec2(position.x - delta, position.y));
	vec3 dv = getFinalPosition(vec2(position.x, position.y + delta)) - getFinalPosition(vec2(position.x, position.y - delta));

	return normalize(cross(du, dv));
}

vec3 getTangent(vec2 position) {
	const float delta = 0.01;
	vec3 du = getFinalPosition(vec2(position.x + delta, position.y)) - getFinalPosition(vec2(position.x - delta, position.y));
	return normalize(du);
}

void main() {
	vec2 position = inPosition;
	position.x += -0.1;
	position.y += -0.1;

	gl_Position = mat * vec4(getFinalPosition(position), 1.0);
	vec3 positionForLight = getFinalPosition(position);


	vec4 positionMv = mMv * vec4(positionForLight, 1.0);
	lightSource = (mMv * vec4(lightSource, 1.0)).xyz;
	vec3 spotLS = (mMv * vec4(spotlightSource, 1.0)).xyz;
	viewDirection = eyePos - positionMv.xyz;
	lightDirection = lightSource - positionMv.xyz;
	spotlightDirection = spotLS - positionMv.xyz;

	if(surfaceMode == 0) {
		vertColor = vec3(position, 0.5);
	} else if(surfaceMode == 1) {
//		vertColor = vec3(red, green, blue);
		vertColor = color1;
	} else if (surfaceMode == 5) {
		vertColor = vec3(position, 0.5);
	} else if (surfaceMode == 2 || surfaceMode == 3) {
		normal = getNormal(inPosition);
	}
	normal = getNormal(inPosition);
	mat3 normalMatrix = inverse(transpose(mat3(mMv)));
//	normal = normalize(normalMatrix * getNormal(inPosition));
	vec3 tangent = normalize(normalMatrix * getTangent(inPosition));
	vec3 binormal = normalize(cross(normal,tangent));

	mat3 mattbnMatrix = mat3(tangent, binormal, normal);

	float NdotL = max(dot(normalize(lightDirection), normal), 0);
	diffuseTotalVert = vec3(NdotL * diffuseLight);

	vec3 halfVector = normalize(normalize(lightDirection) + normalize(viewDirection));
	float NdotHV = max(dot(normal, halfVector), 0);
	specularTotalVert = vec3(pow(NdotHV, 50) * specularLight);


//	surfaceModeOut = surfaceMode;
//	vertColor = inNormal * 0.5 + 0.5;
	distance = length(lightSource);
	texCoord = inPosition;
//	vertColor = vec3(inPosition);
}
