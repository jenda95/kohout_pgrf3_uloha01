#version 420
in vec2 texCoord;

layout (location=0) out vec4 outColor0;

out float ssaoColor;

layout (binding=0) uniform sampler2D positionTexture;
layout (binding=1) uniform sampler2D normalTexture;
layout (binding=2) uniform sampler2D randomTexture;
layout (binding=3) uniform sampler2D noiseTexture;
uniform mat4 projectionMatrix;

const float MAX_KERNEL_SIZE = 64;

void main()
{
    vec3 position = texture(positionTexture, texCoord).xyz;
    vec3 normal = normalize(texture(normalTexture, texCoord).xyz);

    float sampleRadius = 0.5;
    float bias = 0.025;
    float AO = 0;

    vec2 noiseScale = textureSize(normalTexture, 0) / textureSize(noiseTexture, 0);
    vec3 rvec = texture(noiseTexture, texCoord * noiseScale).xyz;
    vec3 tangent = normalize(rvec - normal * dot(rvec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 tbnMatrix = mat3(tangent, bitangent, normal);

    for (int i = 0; i < MAX_KERNEL_SIZE; i++) {
        float index = i * (1.0 / MAX_KERNEL_SIZE);

        vec3 sampleOffset = texture(randomTexture, vec2(index, 0)).rgb;
        sampleOffset = tbnMatrix * sampleOffset;

        vec4 samplePosition = vec4(position + sampleRadius * sampleOffset, 1.0);
        vec4 offset = projectionMatrix * samplePosition; // Projekce
        offset.xyz /= offset.w; // Dehomogenizace
        offset.xyz = offset.xyz * 0.5 + 0.5; // Transformace do souradnic textury - do rozsahu <0;1>

        float occluderPos = texture(positionTexture, offset.xy).z;

        float rangeCheck = smoothstep(0.0, 1.0, sampleRadius / abs(position.z - occluderPos));
        AO += (occluderPos >= (samplePosition.z + bias) ? 1 : 0) * rangeCheck;
    }
    AO = 1.0 - clamp(AO / MAX_KERNEL_SIZE, 0.0, 1.0);
    outColor0 = vec4(vec3(AO), 1);

    ssaoColor = AO;
}
