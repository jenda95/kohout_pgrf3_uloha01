#version 150
in vec2 inPosition; // input from the vertex buffer
in vec3 inColor; // input from the vertex buffer
uniform int function;
out vec3 vertColor; // output from this shader to the next pipeline stage
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 mat;
const float PI = 3.1415;

uniform double audioData; // audiodata to transform graphics

vec3 getSpherical(vec2 position) {
    float azimuth = position.x * 2.0 * PI;
    float zenith = position.y * PI - PI/2.0;
    float r;
    //	int function = 2;
    if (function == 2) {
        r = 1;
        azimuth += time / 20;
    } else {
        r = cos(4 * azimuth);
    }

    vec3 positionWithZ;
    positionWithZ.x= cos(azimuth) * cos(zenith);
    positionWithZ.y= r * sin(azimuth) * cos(zenith);
    positionWithZ.z= r * sin(zenith);

    return positionWithZ;
}

vec3 getCartesian(vec2 position) {
    if (function == 0) {
        return 1.5 * vec3(position.x, position.y, 0.8);
    } else {
        return 1.3 * vec3(position.x * 2 - 1, position.y * 2 - 1, 0.3 * sin(position.x + position.y + time*100) + 0.7);
    }
}

vec3 getCylindrical(vec2 position) {
    float s = position.x * PI * 2.0;
    float t = position.y * PI * 2.0;
    float r;

    if (function == 4) {
        r = 2.0 + cos(2.0 * t);
    } else {
        r = 2.0 + cos(2.0 * t * (sin(time/8))) * sin(s);
    }

    vec3 positionWithZ;
    positionWithZ.x = cos(s) * r;
    positionWithZ.y = sin(s) * r;
    positionWithZ.z = t - 2;
    return positionWithZ / 4.0;
}

vec3 getFinalPosition(vec2 position) {
    vec3 finalPosition;
    if (function < 2) {
        finalPosition = getCartesian(position);
    } else if (function < 4) {
        finalPosition = getSpherical(position);
    } else {
        finalPosition = getCylindrical(position);
    }
    return finalPosition;
}

void main() {
    vec2 position = inPosition;
    //	uniform int function = functionMode;
    //	function = 2;
    //	position.x += -0.1;
    //	position.y += cos(position.x + time/2);
    position.x += -0.1;
    position.y += -0.1;


    //	gl_Position = vec4(position, 0.0, 1.0);
    gl_Position = mat * vec4(getFinalPosition(position), 1.0);
    vertColor = vec3(position, 0.5);
}

