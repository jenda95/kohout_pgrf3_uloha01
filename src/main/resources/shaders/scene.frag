#version 410
in vec4 positionViewSpace;
in vec3 normalToFrag;
in vec3 normalViewSpace;
in vec2 texCoord;

uniform sampler2D textureSample;
uniform int surfaceMode;
uniform vec3 sampleColor;


layout (location=0) out vec4 outColor0;
layout (location=1) out vec4 outColor1;
layout (location=2) out vec4 outColor2;
layout (location=3) out vec4 outColor3;

void main()
{
    outColor0 = vec4(positionViewSpace.xyz, 1);
    outColor1 = vec4(normalize(normalToFrag), 1);
    outColor2 = vec4(normalize(normalViewSpace), 1);
    if(surfaceMode == 0){
        outColor3 = texture(textureSample, texCoord);
    } else if( surfaceMode == 1) {
        outColor3 = vec4(sampleColor,1.0);
    } else if( surfaceMode == 2) {
        outColor3 = vec4(normalize(normalViewSpace), 1);
    } else {
        outColor3 = texture(textureSample, texCoord) + vec4(normalize(normalViewSpace), 1);
    }
}