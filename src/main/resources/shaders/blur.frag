#version 420
in vec2 texCoord;
out vec4 outColor;

layout (binding=0) uniform sampler2D positionTexture;

uniform float blurIntensity;

void main() {
    float blurSize = 16;
    blurSize = blurIntensity;
    vec2 texelSize = 1.0 / vec2(textureSize(positionTexture, 0));
    float result = 0.0;
    vec2 hlim = vec2(float(-blurSize)* 0.5 + 0.5);
    for(int i = 0; i < blurSize; ++i) {
        for(int j = 0; j < blurSize; ++j) {
            vec2 offset = (hlim + vec2(float(i), float(j))) * texelSize;
            result += texture(positionTexture, texCoord + offset).r;
        }
    }
    float fResult = result / float(blurSize * blurSize);
    outColor = vec4(fResult, fResult,fResult,1.0);
}
