#version 410
in vec2 inPosition;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform float time;
uniform int function;
uniform float rotate;

out vec4 positionViewSpace;
out vec3 normalViewSpace;
out vec3 normalToFrag;
out vec2 texCoord;

const float PI = 3.1415;

vec3 getCartesian(vec2 position)
{
    if (function == 0) {
        return 1.5 * vec3(position.x, position.y, 0.8);
    } else {
        return 1.3 * vec3(position.x * 2 - 1, position.y * 2 - 1, 0.3 * sin(position.x + position.y + time / 4) + 0.7);
    }
}

vec3 getSpherical(vec2 position)
{
    float azimuth = position.x * 2.0 * PI;
    float zenith = position.y * PI - PI/2.0;
    float r;
    if (function == 2) {
        r = 1;
        azimuth += time / 20;
    } else {
        r = cos(4 * azimuth);
    }
    vec3 positionWithZ;
    positionWithZ.x= cos(azimuth) * cos(zenith);
    positionWithZ.y= r * sin(azimuth) * cos(zenith);
    positionWithZ.z= r * sin(zenith);
    return positionWithZ;
}

vec3 getCylindrical(vec2 position)
{
    float s = position.x * PI * 2.0;
    float t = position.y * PI * 2.0;
    float r;
    if (function == 4) {
        r = 2.0 + cos(2.0 * t);
    } else {
        r = 2.0 + cos(2.0 * t * (sin(time/8))) * sin(s);
    }
    vec3 positionWithZ;
    positionWithZ.x = cos(s) * r;
    positionWithZ.y = sin(s) * r;
    positionWithZ.z = t - 2;
    return positionWithZ / 4.0;
}

vec3 getFinalPosition(vec2 position)
{
    vec3 finalPosition;
    if (function == 0 || function == 1) {
        finalPosition = getCartesian(position);
    } else if (function == 2 || function == 3) {
        finalPosition = getSpherical(position);
    } else if (function == 4 || function == 5){
        finalPosition = getCylindrical(position);
    }
    return finalPosition;
}

vec3 getNormal(vec2 position)
{
    const float delta = 0.01;
    vec3 du = getFinalPosition(vec2(position.x + delta, position.y)) - getFinalPosition(vec2(position.x - delta, position.y));
    vec3 dv = getFinalPosition(vec2(position.x, position.y + delta)) - getFinalPosition(vec2(position.x, position.y - delta));
    return normalize(cross(du, dv));
}

void main()
{
    texCoord = inPosition;
    vec3 position = getFinalPosition(inPosition);
    vec3 normal = getNormal(inPosition);
    normalToFrag = normal;
    normalViewSpace = transpose(inverse(mat3(viewMatrix))) * normalToFrag;
    vec4 pos4 = vec4(position, 1.0);
    gl_Position = projectionMatrix * viewMatrix * pos4;
    positionViewSpace = viewMatrix * pos4;
}