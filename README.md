# kohout_pgrf3_uloha01

Example of programing in lwjgl OpenGL computer graphics at University of Hradec Kralove

Copyright 2014 - 2020
Jan Kohout (jan.kohout@uhk.cz) at University of Hradec Kralove

This file is part of PGRF_03 project.

GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with PGRF_03 project. If not, see <http://www.gnu.org/licenses/>.

